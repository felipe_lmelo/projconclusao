import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import org.modcs.tools.parser.model.ExecutionRuntime;
import org.modcs.tools.parser.model.Script;
import org.modcs.tools.parser.model.metrics.Metric;

public class experimentos {
    public static void main(String[] args) {

        try {
            ArrayList ups = new ArrayList();
            ArrayList subpainel = new ArrayList();
            ArrayList powerstrip = new ArrayList();
            ArrayList transformer = new ArrayList();
            ArrayList sts = new ArrayList();
            
            //instancia a class calculo que realizar o calculo com o mercury
            calcula calculo = new calcula(); 
            
            BufferedReader buffer = new BufferedReader(new FileReader("C://Users/felip/Desktop/Components270000g1.txt"));
            String linha = null;
            while ((linha = buffer.readLine()) != null) {
                String[] linhaSeparado = linha.split(";");
                String equipamento = linhaSeparado[1];
                if (equipamento.equals("UPS_5KVA0")) {
                    ups.add(linhaSeparado);
                }
                if (equipamento.equals("SDTransformer2")) {
                    transformer.add(linhaSeparado);
                }
                if (equipamento.equals("Subpanel5")) {
                    subpainel.add(linhaSeparado);
                }
                if (equipamento.equals("PowerStrip6")) {
                    powerstrip.add(linhaSeparado);
                }
                
                //pegar as informações do dispositivo STS
                    if (equipamento.equals("STS4")) {
                        sts.add(linhaSeparado);
                    }
            }
            int x = 0;
            int count = 0;
            for (int u = 0; u < ups.size(); ++u) {
                for (int t = 0; t < transformer.size(); ++t) {
                    for (int s = 0; s < subpainel.size(); ++s) {
                        for (int p = 0; p < powerstrip.size(); ++p) {
                             for (int st = 0; st < sts.size(); ++st) {
                            ArrayList lista = new ArrayList();
                            ArrayList subLista = new ArrayList();
	        			      
                            String[] equipamentoUps = (String[]) ups.get(u);
                            String[] equipamentoTransformer = (String[]) transformer.get(t);
                            String[] equipamentoSubpainel = (String[]) subpainel.get(s);
                            String[] equipamentoPowerstrip = (String[]) powerstrip.get(p);
                            String[] equipamentoSTS = (String[]) sts.get(st);
                            
                           double resultado = calculo.executarScript(equipamentoUps[3], equipamentoTransformer[3], equipamentoSubpainel[3], equipamentoPowerstrip[3], equipamentoUps[1], equipamentoTransformer[1], equipamentoSubpainel[1], equipamentoPowerstrip[1]);
                            
                           /* BufferedWriter bw = new BufferedWriter(new FileWriter("ResultadosDisponibilidadeA1_nnn.txt", true));
                            bw.write("Experimento " + count + "; UPS " + u + "; SDT " + t + "; Subpanel " + s + "; PowerStrip " + p + "; " + resultado + "; " 
                                    + Math.log10(1-resultado) +"\n");
                            
                            bw.close();
                           */
                            //A2
                            /*double resultadoA2 = calculo.executarScriptA2(equipamentoUps[3], equipamentoTransformer[3], equipamentoSubpainel[3], equipamentoPowerstrip[3], equipamentoSTS[3], equipamentoUps[1], equipamentoTransformer[1], equipamentoSubpainel[1], equipamentoPowerstrip[1], equipamentoSTS[1]);
                            
                            BufferedWriter A2 = new BufferedWriter(new FileWriter("ResultadosDisponibilidadeA2.txt", true));
                            A2.write("Experimento " + count + "; UPS " + u + "; UPS " + u + "; SDT " + t + "; Subpanel " + s + "; PowerStrip " + p + "; STS  " + st + " ; "+ resultadoA2 + "; " 
                                    + Math.log10(1-resultadoA2) +"\n");
                            
                            A2.close();
                            /*
                            //A3*/
                           /*
                            double resultadoA3 = calculo.executarScriptA3(equipamentoUps[3], equipamentoTransformer[3], equipamentoSubpainel[3], equipamentoPowerstrip[3], equipamentoSTS[3], equipamentoUps[1], equipamentoTransformer[1], equipamentoSubpainel[1], equipamentoPowerstrip[1], equipamentoSTS[1]);
                            
                            BufferedWriter A3 = new BufferedWriter(new FileWriter("ResultadosDisponibilidadeA3.txt", true));
                            A3.write("Experimento " + count + "; UPS " + u + "; UPS " + u + "; SDT " + t + "; Subpanel " + s + "; PowerStrip " + p + "; STS " + st + " ; " +resultadoA3 + "; " 
                                    + Math.log10(1-resultadoA3) +"\n");
                            
                            A3.close();
                            
                             //A4
                           */
                            double resultadoA4 = calculo.executarScriptA4(equipamentoUps[3], equipamentoTransformer[3], equipamentoSubpainel[3], equipamentoPowerstrip[3], equipamentoSTS[3], equipamentoUps[1], equipamentoTransformer[1], equipamentoSubpainel[1], equipamentoPowerstrip[1], equipamentoSTS[1]);
                            
                            BufferedWriter A4 = new BufferedWriter(new FileWriter("ResultadosDisponibilidadeA4.txt", true));
                            A4.write("Experimento " + count + "; UPS " + u + "; UPS " + u + "; SDT " + t + "; Subpanel " + s + "; PowerStrip " + p + "; STS " + st + " ; "  + resultadoA4 + "; " 
                                    + Math.log10(1-resultadoA4) +"\n");
                            
                            A4.close();
                            
                            //A4*/ 
                            /*double resultadoA5 = calculo.executarScriptA5(equipamentoUps[3], equipamentoTransformer[3], equipamentoSubpainel[3], equipamentoPowerstrip[3], "5545", equipamentoUps[1], equipamentoTransformer[1], equipamentoSubpainel[1], equipamentoPowerstrip[1], "sts");
                            
                            BufferedWriter A5 = new BufferedWriter(new FileWriter("ResultadosDisponibilidadeA555555555.txt", true));
                            A5.write("Experimento " + count + "; UPS " + u + "; UPS " + u + "; SDT " + t + "; Subpanel " + s + "; PowerStrip " + p + " ; " + resultadoA5 + "; " 
                                    + Math.log10(1-resultadoA5) +"\n");
                            
                            A5.close(); */
                            count++;
                          
                             }
                      }
                    }
                }
             }
        } catch (IOException e) {
            System.out.print("Erro na abertura do arquivo: %s.\n");
        }

        System.out.println();
    }
    private String arq;
}
