
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import org.modcs.tools.parser.model.ExecutionRuntime;
import org.modcs.tools.parser.model.Script;
import org.modcs.tools.parser.model.metrics.Metric;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author fmelo
 */
public class calcula {
        
    static String script(String equipamentoUps, String equipamentoTransformer, String equipamentoSubpainel, String equipamentoPowerstrip, String ups, String sdt, String subpainel, String powerstrip) {

        return "SPN Model{ \n"
                + "place " + ups + "_on( tokens= 1 ); \n"
                + "place " + ups + "_off; \n"
                + "place " + sdt + "_on( tokens= 1 ); \n"
                + "place " + sdt + "_off; \n"
                + "place " + subpainel + "_on( tokens= 1 ); \n"
                + "place " + subpainel + "_off; \n"
                + "place " + powerstrip + "_on( tokens= 1 ); \n"
                + "place " + powerstrip + "_off; \n"
                + "timedTransition failure" + ups.substring(0, 1).toUpperCase() + ups.substring(1) + "(\n"
                + "inputs = [" + ups + "_on],\n"
                + "outputs = [" + ups + "_off],\n"
                + "delay = " + equipamentoUps + ");\n"
                + "timedTransition repair" + ups.substring(0, 1).toUpperCase() + ups.substring(1) + "(\n"
                + "inputs = [" + ups + "_off],\n"
                + "outputs = [" + ups + "_on],\n"
                + "delay = 8.0);\n"
                + "timedTransition failure" + sdt.substring(0, 1).toUpperCase() + sdt.substring(1) + "(\n"
                + "inputs = [" + sdt + "_on],\n"
                + "outputs = [" + sdt + "_off],\n"
                + "delay = " + equipamentoTransformer + ");\n"
                + "timedTransition repair" + sdt.substring(0, 1).toUpperCase() + sdt.substring(1) + "(\n"
                + "inputs = [" + sdt + "_off],\n"
                + "outputs = [" + sdt + "_on],\n"
                + "delay = 8.0);\n"
                + "timedTransition failure" + subpainel.substring(0, 1).toUpperCase() + subpainel.substring(1) + "(\n"
                + "inputs = [" + subpainel + "_on],\n"
                + "outputs = [" + subpainel + "_off],\n"
                + "delay = " + equipamentoSubpainel + ");\n"
                + "timedTransition repair" + subpainel.substring(0, 1).toUpperCase() + subpainel.substring(1) + "(\n"
                + "inputs = [" + subpainel + "_off],\n"
                + "outputs = [" + subpainel + "_on],\n"
                + "delay = 8.0);\n"
                + "timedTransition failure" + powerstrip.substring(0, 1).toUpperCase() + powerstrip.substring(1) + "(\n"
                + "inputs = [" + powerstrip + "_on],\n"
                + "outputs = [" + powerstrip + "_off],\n"
                + "delay = " + equipamentoPowerstrip + ");\n"
                + "timedTransition repair" + powerstrip.substring(0, 1).toUpperCase() + powerstrip.substring(1) + "(\n"
                + "inputs = [" + powerstrip + "_off],\n"
                + "outputs = [" + powerstrip + "_on],\n"
                + "delay = 8.0);\n"
                + "metric av = stationaryAnalysis( expression = \"P{((#" + ups + "_on=1)AND(#" + sdt + "_on=1)AND(#" + subpainel + "_on=1)AND(#" + powerstrip + "_on=1))}\");}\n"
                + "main {\n"
                + "av = solve( Model,av );\n"
                + "println(av);}\n";
    }
    
    static double executarScript(String equipamentoUps, String equipamentoTransformer, String equipamentoSubpainel, String equipamentoPowerstrip, String ups, String sdt, String subpainel, String powerstrip) throws IOException {
       
        ExecutionRuntime runtime = new ExecutionRuntime();
        //System.out.println(script(equipamentoUps, ups));
        Script script;
        script = new Script(script(equipamentoUps, equipamentoTransformer, equipamentoSubpainel, equipamentoPowerstrip, ups, sdt, subpainel, powerstrip));
        runtime.evaluateScript(script);
        
        

        double av;
        Metric Aval;

        Aval = runtime.getModel("Model").getMetric("av");
        av = Aval.solve();
        
        //escrever no arquivo
        //BufferedWriter bw = new BufferedWriter(new FileWriter("teste.txt", true));
        
        // Escreve e fecha arquivo
        //bw.write(av + "\t");
        //bw.close();
        return av;
    }
    
    static String scriptSimplificado(String equipamentoUps, String ups) {

        return "SPN Model{ \n"
                + "place " + ups + "_on( tokens= 1 ); \n"
                + "place " + ups + "_off; \n"
                + "timedTransition failure" + ups.substring(0, 1).toUpperCase() + ups.substring(1) + "(\n"
                + "inputs = [" + ups + "_on],\n"
                + "outputs = [" + ups + "_off],\n"
                + "delay = " + equipamentoUps + ");\n"
                + "timedTransition repair" + ups.substring(0, 1).toUpperCase() + ups.substring(1) + "(\n"
                + "inputs = [" + ups + "_off],\n"
                + "outputs = [" + ups + "_on],\n"
                + "delay = 8.0);\n"
                + "metric av = stationaryAnalysis( expression = \"P{(#" + ups + "_on=1)}\");}\n"
                + "main {\n"
                + "av = solve( Model,av );\n"
                + "println(av);}\n";
    }
    
      static double executarScriptSimplificado(String equipamentoUps, String ups) throws IOException {
       
        ExecutionRuntime runtime = new ExecutionRuntime();
        //System.out.println(script(equipamentoUps, ups));
        Script script = new Script(scriptSimplificado(equipamentoUps, ups));
        runtime.evaluateScript(script);

        double av;
        Metric Aval;

        Aval = runtime.getModel("Model").getMetric("av");
        av = Aval.solve();
        
        //escrever no arquivo
       // BufferedWriter bw = new BufferedWriter(new FileWriter("teste.txt", true));
        
        // Escreve e fecha arquivo
        //bw.write(av + "\t");
       // bw.close();
        return av;
    }
      
    static String scriptA2(String equipamentoUps, String equipamentoTransformer, String equipamentoSubpainel, String equipamentoPowerstrip, String equipamentoSts, String ups, String sdt, String subpainel, String powerstrip, String sts) {

        return "SPN Model{ \n"
                + "place " + ups + "_on( tokens= 1 ); \n"
                + "place " + ups + "_off; \n"
                + "place " + ups + "_on2( tokens= 1 ); \n"
                + "place " + ups + "_off2; \n"
                + "place " + sts + "_on( tokens= 1 ); \n"
                + "place " + sts + "_off; \n"
                + "place " + sdt + "_on( tokens= 1 ); \n"
                + "place " + sdt + "_off; \n"
                + "place " + subpainel + "_on( tokens= 1 ); \n"
                + "place " + subpainel + "_off; \n"
                + "place " + powerstrip + "_on( tokens= 1 ); \n"
                + "place " + powerstrip + "_off; \n"
                + "timedTransition failure" + ups.substring(0, 1).toUpperCase() + ups.substring(1) + "(\n"
                + "inputs = [" + ups + "_on],\n"
                + "outputs = [" + ups + "_off],\n"
                + "delay = " + equipamentoUps + ");\n"
                + "timedTransition repair" + ups.substring(0, 1).toUpperCase() + ups.substring(1) + "(\n"
                + "inputs = [" + ups + "_off],\n"
                + "outputs = [" + ups + "_on],\n"
                + "delay = 8.0);\n"
                + "timedTransition failure" + ups.substring(0, 1).toUpperCase() + ups.substring(1) + "2(\n"
                + "inputs = [" + ups + "_on2],\n"
                + "outputs = [" + ups + "_off2],\n"
                + "delay = " + equipamentoUps + ");\n"
                + "timedTransition repair" + ups.substring(0, 1).toUpperCase() + ups.substring(1) + "2(\n"
                + "inputs = [" + ups + "_off2],\n"
                + "outputs = [" + ups + "_on2],\n"
                + "delay = 8.0);\n"
                + "timedTransition failure" + sts.substring(0, 1).toUpperCase() + sts.substring(1) + "2(\n"
                + "inputs = [" + sts + "_on],\n"
                + "outputs = [" + sts + "_off],\n"
                + "delay = " + equipamentoSts + ");\n"
                + "timedTransition repair" + sts.substring(0, 1).toUpperCase() + sts.substring(1) + "2(\n"
                + "inputs = [" + sts + "_off],\n"
                + "outputs = [" + sts + "_on],\n"
                + "delay = 8.0);\n"           
                + "timedTransition failure" + sdt.substring(0, 1).toUpperCase() + sdt.substring(1) + "(\n"
                + "inputs = [" + sdt + "_on],\n"
                + "outputs = [" + sdt + "_off],\n"
                + "delay = " + equipamentoTransformer + ");\n"
                + "timedTransition repair" + sdt.substring(0, 1).toUpperCase() + sdt.substring(1) + "(\n"
                + "inputs = [" + sdt + "_off],\n"
                + "outputs = [" + sdt + "_on],\n"
                + "delay = 8.0);\n"
                + "timedTransition failure" + subpainel.substring(0, 1).toUpperCase() + subpainel.substring(1) + "(\n"
                + "inputs = [" + subpainel + "_on],\n"
                + "outputs = [" + subpainel + "_off],\n"
                + "delay = " + equipamentoSubpainel + ");\n"
                + "timedTransition repair" + subpainel.substring(0, 1).toUpperCase() + subpainel.substring(1) + "(\n"
                + "inputs = [" + subpainel + "_off],\n"
                + "outputs = [" + subpainel + "_on],\n"
                + "delay = 8.0);\n"
                + "timedTransition failure" + powerstrip.substring(0, 1).toUpperCase() + powerstrip.substring(1) + "(\n"
                + "inputs = [" + powerstrip + "_on],\n"
                + "outputs = [" + powerstrip + "_off],\n"
                + "delay = " + equipamentoPowerstrip + ");\n"
                + "timedTransition repair" + powerstrip.substring(0, 1).toUpperCase() + powerstrip.substring(1) + "(\n"
                + "inputs = [" + powerstrip + "_off],\n"
                + "outputs = [" + powerstrip + "_on],\n"
                + "delay = 8.0);\n"
                + "metric av = stationaryAnalysis( expression = \"P{((#" + ups + "_on=1)OR(#" + ups + "_on2=1))AND(#" + sts + "_on=1)AND(#" + sdt + "_on=1)AND(#" + subpainel + "_on=1)AND(#" + powerstrip + "_on=1)}\");}\n"
                + "main {\n"
                + "av = solve( Model,av );\n"
                + "println(av);}\n";
    }
    
        static double executarScriptA2(String equipamentoUps, String equipamentoTransformer, String equipamentoSubpainel, String equipamentoPowerstrip, String equipamentoSts, String ups, String sdt, String subpainel, String powerstrip, String sts) throws IOException {
       
        ExecutionRuntime runtime = new ExecutionRuntime();
        //System.out.println(script(equipamentoUps, ups));
        Script script;
        script = new Script(scriptA2(equipamentoUps, equipamentoTransformer, equipamentoSubpainel, equipamentoPowerstrip, equipamentoSts, ups, sdt, subpainel, powerstrip, sts));
        //System.out.println(script);
        //System.exit(0);
        runtime.evaluateScript(script);

        double av;
        Metric Aval;

        Aval = runtime.getModel("Model").getMetric("av");
        av = Aval.solve();
        
        //escrever no arquivo
        //BufferedWriter bw = new BufferedWriter(new FileWriter("teste.txt", true));
        
        // Escreve e fecha arquivo
        //bw.write(av + "\t");
        //bw.close();
        return av;
    }
        
    static String scriptA3(String equipamentoUps, String equipamentoTransformer, String equipamentoSubpainel, String equipamentoPowerstrip, String equipamentoSts, String ups, String sdt, String subpainel, String powerstrip, String sts) {

        return "SPN Model{ \n"
                + "place " + ups + "_on( tokens= 1 ); \n"
                + "place " + ups + "_off; \n"
                + "place " + ups + "_on2( tokens= 1 ); \n"
                + "place " + ups + "_off2; \n"
                + "place " + sts + "_on( tokens= 1 ); \n"
                + "place " + sts + "_off; \n"
                + "place " + sdt + "_on( tokens= 1 ); \n"
                + "place " + sdt + "_off; \n"
                + "place " + sdt + "_on2( tokens= 1 ); \n"
                + "place " + sdt + "_off2; \n"
                + "place " + subpainel + "_on( tokens= 1 ); \n"
                + "place " + subpainel + "_off; \n"
                + "place " + powerstrip + "_on( tokens= 1 ); \n"
                + "place " + powerstrip + "_off; \n"
                + "timedTransition failure" + ups.substring(0, 1).toUpperCase() + ups.substring(1) + "(\n"
                + "inputs = [" + ups + "_on],\n"
                + "outputs = [" + ups + "_off],\n"
                + "delay = " + equipamentoUps + ");\n"
                + "timedTransition repair" + ups.substring(0, 1).toUpperCase() + ups.substring(1) + "(\n"
                + "inputs = [" + ups + "_off],\n"
                + "outputs = [" + ups + "_on],\n"
                + "delay = 8.0);\n"
                + "timedTransition failure" + ups.substring(0, 1).toUpperCase() + ups.substring(1) + "2(\n"
                + "inputs = [" + ups + "_on2],\n"
                + "outputs = [" + ups + "_off2],\n"
                + "delay = " + equipamentoUps + ");\n"
                + "timedTransition repair" + ups.substring(0, 1).toUpperCase() + ups.substring(1) + "2(\n"
                + "inputs = [" + ups + "_off2],\n"
                + "outputs = [" + ups + "_on2],\n"
                + "delay = 8.0);\n"
                + "timedTransition failure" + sts.substring(0, 1).toUpperCase() + sts.substring(1) + "2(\n"
                + "inputs = [" + sts + "_on],\n"
                + "outputs = [" + sts + "_off],\n"
                + "delay = " + equipamentoSts + ");\n"
                + "timedTransition repair" + sts.substring(0, 1).toUpperCase() + sts.substring(1) + "2(\n"
                + "inputs = [" + sts + "_off],\n"
                + "outputs = [" + sts + "_on],\n"
                + "delay = 8.0);\n"           
                + "timedTransition failure" + sdt.substring(0, 1).toUpperCase() + sdt.substring(1) + "(\n"
                + "inputs = [" + sdt + "_on],\n"
                + "outputs = [" + sdt + "_off],\n"
                + "delay = " + equipamentoTransformer + ");\n"
                + "timedTransition repair" + sdt.substring(0, 1).toUpperCase() + sdt.substring(1) + "(\n"
                + "inputs = [" + sdt + "_off],\n"
                + "outputs = [" + sdt + "_on],\n"
                + "delay = 8.0);\n"
                + "timedTransition failure" + sdt.substring(0, 1).toUpperCase() + sdt.substring(1) + "2(\n"
                + "inputs = [" + sdt + "_on2],\n"
                + "outputs = [" + sdt + "_off2],\n"
                + "delay = " + equipamentoTransformer + ");\n"
                + "timedTransition repair" + sdt.substring(0, 1).toUpperCase() + sdt.substring(1) + "2(\n"
                + "inputs = [" + sdt + "_off2],\n"
                + "outputs = [" + sdt + "_on2],\n"
                + "delay = 8.0);\n"
                + "timedTransition failure" + subpainel.substring(0, 1).toUpperCase() + subpainel.substring(1) + "(\n"
                + "inputs = [" + subpainel + "_on],\n"
                + "outputs = [" + subpainel + "_off],\n"
                + "delay = " + equipamentoSubpainel + ");\n"
                + "timedTransition repair" + subpainel.substring(0, 1).toUpperCase() + subpainel.substring(1) + "(\n"
                + "inputs = [" + subpainel + "_off],\n"
                + "outputs = [" + subpainel + "_on],\n"
                + "delay = 8.0);\n"
                + "timedTransition failure" + powerstrip.substring(0, 1).toUpperCase() + powerstrip.substring(1) + "(\n"
                + "inputs = [" + powerstrip + "_on],\n"
                + "outputs = [" + powerstrip + "_off],\n"
                + "delay = " + equipamentoPowerstrip + ");\n"
                + "timedTransition repair" + powerstrip.substring(0, 1).toUpperCase() + powerstrip.substring(1) + "(\n"
                + "inputs = [" + powerstrip + "_off],\n"
                + "outputs = [" + powerstrip + "_on],\n"
                + "delay = 8.0);\n"
                + "metric av = stationaryAnalysis( expression = \"P{(((#" + ups + "_on=1)AND(#" + sdt + "_on=1))OR((#" + ups + "_on2=1)AND(#" + sdt + "_on2=1)))AND(#" + sts + "_on=1)AND(#" + subpainel + "_on=1)AND(#" + powerstrip + "_on=1)}\");}\n"
                + "main {\n"
                + "av = solve( Model,av );\n"
                + "println(av);}\n";
    }
    
        static double executarScriptA3(String equipamentoUps, String equipamentoTransformer, String equipamentoSubpainel, String equipamentoPowerstrip, String equipamentoSts, String ups, String sdt, String subpainel, String powerstrip, String sts) throws IOException {
       
        ExecutionRuntime runtime = new ExecutionRuntime();
        //System.out.println(script(equipamentoUps, ups));
        Script script;
        script = new Script(scriptA3(equipamentoUps, equipamentoTransformer, equipamentoSubpainel, equipamentoPowerstrip, equipamentoSts, ups, sdt, subpainel, powerstrip, sts));
        //System.out.println(script);
        //System.exit(0);
        runtime.evaluateScript(script);

        double av;
        Metric Aval;

        Aval = runtime.getModel("Model").getMetric("av");
        av = Aval.solve();
        
        //escrever no arquivo
        //BufferedWriter bw = new BufferedWriter(new FileWriter("teste.txt", true));
        
        // Escreve e fecha arquivo
        //bw.write(av + "\t");
        //bw.close();
        return av;
    }    
        
    static String scriptA4(String equipamentoUps, String equipamentoTransformer, String equipamentoSubpainel, String equipamentoPowerstrip, String equipamentoSts, String ups, String sdt, String subpainel, String powerstrip, String sts) {

        return "SPN Model{ \n"
                + "place " + ups + "_on( tokens= 1 ); \n"
                + "place " + ups + "_off; \n"
                + "place " + ups + "_on2( tokens= 1 ); \n"
                + "place " + ups + "_off2; \n"
                + "place " + sts + "_on( tokens= 1 ); \n"
                + "place " + sts + "_off; \n"
                + "place " + sdt + "_on( tokens= 1 ); \n"
                + "place " + sdt + "_off; \n"
                + "place " + sdt + "_on2( tokens= 1 ); \n"
                + "place " + sdt + "_off2; \n"
                + "place " + subpainel + "_on( tokens= 1 ); \n"
                + "place " + subpainel + "_off; \n"
                + "place " + subpainel + "_on2( tokens= 1 ); \n"
                + "place " + subpainel + "_off2; \n"
                + "place " + powerstrip + "_on( tokens= 1 ); \n"
                + "place " + powerstrip + "_off; \n"
                + "timedTransition failure" + ups.substring(0, 1).toUpperCase() + ups.substring(1) + "(\n"
                + "inputs = [" + ups + "_on],\n"
                + "outputs = [" + ups + "_off],\n"
                + "delay = " + equipamentoUps + ");\n"
                + "timedTransition repair" + ups.substring(0, 1).toUpperCase() + ups.substring(1) + "(\n"
                + "inputs = [" + ups + "_off],\n"
                + "outputs = [" + ups + "_on],\n"
                + "delay = 8.0);\n"
                + "timedTransition failure" + ups.substring(0, 1).toUpperCase() + ups.substring(1) + "2(\n"
                + "inputs = [" + ups + "_on2],\n"
                + "outputs = [" + ups + "_off2],\n"
                + "delay = " + equipamentoUps + ");\n"
                + "timedTransition repair" + ups.substring(0, 1).toUpperCase() + ups.substring(1) + "2(\n"
                + "inputs = [" + ups + "_off2],\n"
                + "outputs = [" + ups + "_on2],\n"
                + "delay = 8.0);\n"
                + "timedTransition failure" + sts.substring(0, 1).toUpperCase() + sts.substring(1) + "2(\n"
                + "inputs = [" + sts + "_on],\n"
                + "outputs = [" + sts + "_off],\n"
                + "delay = " + equipamentoSts + ");\n"
                + "timedTransition repair" + sts.substring(0, 1).toUpperCase() + sts.substring(1) + "2(\n"
                + "inputs = [" + sts + "_off],\n"
                + "outputs = [" + sts + "_on],\n"
                + "delay = 8.0);\n"           
                + "timedTransition failure" + sdt.substring(0, 1).toUpperCase() + sdt.substring(1) + "(\n"
                + "inputs = [" + sdt + "_on],\n"
                + "outputs = [" + sdt + "_off],\n"
                + "delay = " + equipamentoTransformer + ");\n"
                + "timedTransition repair" + sdt.substring(0, 1).toUpperCase() + sdt.substring(1) + "(\n"
                + "inputs = [" + sdt + "_off],\n"
                + "outputs = [" + sdt + "_on],\n"
                + "delay = 8.0);\n"
                + "timedTransition failure" + sdt.substring(0, 1).toUpperCase() + sdt.substring(1) + "2(\n"
                + "inputs = [" + sdt + "_on2],\n"
                + "outputs = [" + sdt + "_off2],\n"
                + "delay = " + equipamentoTransformer + ");\n"
                + "timedTransition repair" + sdt.substring(0, 1).toUpperCase() + sdt.substring(1) + "2(\n"
                + "inputs = [" + sdt + "_off2],\n"
                + "outputs = [" + sdt + "_on2],\n"
                + "delay = 8.0);\n"
                + "timedTransition failure" + subpainel.substring(0, 1).toUpperCase() + subpainel.substring(1) + "(\n"
                + "inputs = [" + subpainel + "_on],\n"
                + "outputs = [" + subpainel + "_off],\n"
                + "delay = " + equipamentoSubpainel + ");\n"
                + "timedTransition repair" + subpainel.substring(0, 1).toUpperCase() + subpainel.substring(1) + "(\n"
                + "inputs = [" + subpainel + "_off],\n"
                + "outputs = [" + subpainel + "_on],\n"
                + "delay = 8.0);\n"
                + "timedTransition failure" + subpainel.substring(0, 1).toUpperCase() + subpainel.substring(1) + "2(\n"
                + "inputs = [" + subpainel + "_on2],\n"
                + "outputs = [" + subpainel + "_off2],\n"
                + "delay = " + equipamentoSubpainel + ");\n"
                + "timedTransition repair" + subpainel.substring(0, 1).toUpperCase() + subpainel.substring(1) + "2(\n"
                + "inputs = [" + subpainel + "_off2],\n"
                + "outputs = [" + subpainel + "_on2],\n"
                + "delay = 8.0);\n"
                + "timedTransition failure" + powerstrip.substring(0, 1).toUpperCase() + powerstrip.substring(1) + "(\n"
                + "inputs = [" + powerstrip + "_on],\n"
                + "outputs = [" + powerstrip + "_off],\n"
                + "delay = " + equipamentoPowerstrip + ");\n"
                + "timedTransition repair" + powerstrip.substring(0, 1).toUpperCase() + powerstrip.substring(1) + "(\n"
                + "inputs = [" + powerstrip + "_off],\n"
                + "outputs = [" + powerstrip + "_on],\n"
                + "delay = 8.0);\n"
                + "metric av = stationaryAnalysis( expression = \"P{(((#" + ups + "_on=1)AND(#" + sdt + "_on=1)AND(#" + subpainel + "_on=1))OR((#" + ups + "_on2=1)AND(#" + sdt + "_on2=1)AND(#" + subpainel + "_on2=1)))AND(#" + sts + "_on=1)AND(#" + powerstrip + "_on=1)}\");}\n"
                + "main {\n"
                + "av = solve( Model,av );\n"
                + "println(av);}\n";
    }
    
        static double executarScriptA4(String equipamentoUps, String equipamentoTransformer, String equipamentoSubpainel, String equipamentoPowerstrip, String equipamentoSts, String ups, String sdt, String subpainel, String powerstrip, String sts) throws IOException {
       
        ExecutionRuntime runtime = new ExecutionRuntime();
        //System.out.println(script(equipamentoUps, ups));
        Script script;
        script = new Script(scriptA4(equipamentoUps, equipamentoTransformer, equipamentoSubpainel, equipamentoPowerstrip, equipamentoSts, ups, sdt, subpainel, powerstrip, sts));
        //System.out.println(script);
        //System.exit(0);s
        runtime.evaluateScript(script);

        double av;
        Metric Aval;

        Aval = runtime.getModel("Model").getMetric("av");
        av = Aval.solve();
        
        //escrever no arquivo
        //BufferedWriter bw = new BufferedWriter(new FileWriter("teste.txt", true));
        
        // Escreve e fecha arquivo
        //bw.write(av + "\t");
        //bw.close();
        return av;
    }      
   static String scriptA5(String equipamentoUps, String equipamentoTransformer, String equipamentoSubpainel, String equipamentoPowerstrip, String equipamentoSts, String ups, String sdt, String subpainel, String powerstrip, String sts) {

        return "SPN Model{ \n"
                + "place " + ups + "_on( tokens= 1 ); \n"
                + "place " + ups + "_off; \n"
                + "place " + ups + "_on2( tokens= 1 ); \n"
                + "place " + ups + "_off2; \n"
                + "place " + sdt + "_on( tokens= 1 ); \n"
                + "place " + sdt + "_off; \n"
                + "place " + sdt + "_on2( tokens= 1 ); \n"
                + "place " + sdt + "_off2; \n"
                + "place " + subpainel + "_on( tokens= 1 ); \n"
                + "place " + subpainel + "_off; \n"
                + "place " + subpainel + "_on2( tokens= 1 ); \n"
                + "place " + subpainel + "_off2; \n"
                + "place " + powerstrip + "_on( tokens= 1 ); \n"
                + "place " + powerstrip + "_off; \n"
                + "place " + powerstrip + "_on2( tokens= 1 ); \n"
                + "place " + powerstrip + "_off2; \n"
                + "timedTransition failure" + ups.substring(0, 1).toUpperCase() + ups.substring(1) + "(\n"
                + "inputs = [" + ups + "_on],\n"
                + "outputs = [" + ups + "_off],\n"
                + "delay = " + equipamentoUps + ");\n"
                + "timedTransition repair" + ups.substring(0, 1).toUpperCase() + ups.substring(1) + "(\n"
                + "inputs = [" + ups + "_off],\n"
                + "outputs = [" + ups + "_on],\n"
                + "delay = 8.0);\n"
                + "timedTransition failure" + ups.substring(0, 1).toUpperCase() + ups.substring(1) + "2(\n"
                + "inputs = [" + ups + "_on2],\n"
                + "outputs = [" + ups + "_off2],\n"
                + "delay = " + equipamentoUps + ");\n"
                + "timedTransition repair" + ups.substring(0, 1).toUpperCase() + ups.substring(1) + "2(\n"
                + "inputs = [" + ups + "_off2],\n"
                + "outputs = [" + ups + "_on2],\n"
                + "delay = 8.0);\n"          
                + "timedTransition failure" + sdt.substring(0, 1).toUpperCase() + sdt.substring(1) + "(\n"
                + "inputs = [" + sdt + "_on],\n"
                + "outputs = [" + sdt + "_off],\n"
                + "delay = " + equipamentoTransformer + ");\n"
                + "timedTransition repair" + sdt.substring(0, 1).toUpperCase() + sdt.substring(1) + "(\n"
                + "inputs = [" + sdt + "_off],\n"
                + "outputs = [" + sdt + "_on],\n"
                + "delay = 8.0);\n"
                + "timedTransition failure" + sdt.substring(0, 1).toUpperCase() + sdt.substring(1) + "2(\n"
                + "inputs = [" + sdt + "_on2],\n"
                + "outputs = [" + sdt + "_off2],\n"
                + "delay = " + equipamentoTransformer + ");\n"
                + "timedTransition repair" + sdt.substring(0, 1).toUpperCase() + sdt.substring(1) + "2(\n"
                + "inputs = [" + sdt + "_off2],\n"
                + "outputs = [" + sdt + "_on2],\n"
                + "delay = 8.0);\n"
                + "timedTransition failure" + subpainel.substring(0, 1).toUpperCase() + subpainel.substring(1) + "(\n"
                + "inputs = [" + subpainel + "_on],\n"
                + "outputs = [" + subpainel + "_off],\n"
                + "delay = " + equipamentoSubpainel + ");\n"
                + "timedTransition repair" + subpainel.substring(0, 1).toUpperCase() + subpainel.substring(1) + "(\n"
                + "inputs = [" + subpainel + "_off],\n"
                + "outputs = [" + subpainel + "_on],\n"
                + "delay = 8.0);\n"
                + "timedTransition failure" + subpainel.substring(0, 1).toUpperCase() + subpainel.substring(1) + "2(\n"
                + "inputs = [" + subpainel + "_on2],\n"
                + "outputs = [" + subpainel + "_off2],\n"
                + "delay = " + equipamentoSubpainel + ");\n"
                + "timedTransition repair" + subpainel.substring(0, 1).toUpperCase() + subpainel.substring(1) + "2(\n"
                + "inputs = [" + subpainel + "_off2],\n"
                + "outputs = [" + subpainel + "_on2],\n"
                + "delay = 8.0);\n"
                + "timedTransition failure" + powerstrip.substring(0, 1).toUpperCase() + powerstrip.substring(1) + "(\n"
                + "inputs = [" + powerstrip + "_on],\n"
                + "outputs = [" + powerstrip + "_off],\n"
                + "delay = " + equipamentoPowerstrip + ");\n"
                + "timedTransition repair" + powerstrip.substring(0, 1).toUpperCase() + powerstrip.substring(1) + "(\n"
                + "inputs = [" + powerstrip + "_off],\n"
                + "outputs = [" + powerstrip + "_on],\n"
                + "delay = 8.0);\n"
                + "timedTransition failure" + powerstrip.substring(0, 1).toUpperCase() + powerstrip.substring(1) + "2(\n"
                + "inputs = [" + powerstrip + "_on2],\n"
                + "outputs = [" + powerstrip + "_off2],\n"
                + "delay = " + equipamentoPowerstrip + ");\n"
                + "timedTransition repair" + powerstrip.substring(0, 1).toUpperCase() + powerstrip.substring(1) + "2(\n"
                + "inputs = [" + powerstrip + "_off2],\n"
                + "outputs = [" + powerstrip + "_on2],\n"
                + "delay = 8.0);\n"                
                + "metric av = stationaryAnalysis( expression = \"P{(((#" + ups + "_on=1)AND(#" + sdt + "_on=1)AND(#" + subpainel + "_on=1)AND(#" + powerstrip + "_on=1))OR((#" + ups + "_on2=1)AND(#" + sdt + "_on2=1)AND(#" + subpainel + "_on2=1)AND(#" + powerstrip + "_on2=1)))}\");}\n"
                + "main {\n"
                + "av = solve( Model,av );\n"
                + "println(av);}\n";
    }
    
        static double executarScriptA5(String equipamentoUps, String equipamentoTransformer, String equipamentoSubpainel, String equipamentoPowerstrip, String equipamentoSts, String ups, String sdt, String subpainel, String powerstrip, String sts) throws IOException {
       
        ExecutionRuntime runtime = new ExecutionRuntime();
        //System.out.println(script(equipamentoUps, ups));
        Script script;
        script = new Script(scriptA5(equipamentoUps, equipamentoTransformer, equipamentoSubpainel, equipamentoPowerstrip, equipamentoSts, ups, sdt, subpainel, powerstrip, sts));
        //System.out.println(script);
        //System.exit(0);s
        runtime.evaluateScript(script);

        double av;
        Metric Aval;

        Aval = runtime.getModel("Model").getMetric("av");
        av = Aval.solve();
        
        //escrever no arquivo
        //BufferedWriter bw = new BufferedWriter(new FileWriter("teste.txt", true));
        
        // Escreve e fecha arquivo
        //bw.write(av + "\t");
        //bw.close();
        return av;
    }  

    double executarScriptA2(double executarScriptA2) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
