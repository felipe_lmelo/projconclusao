/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author felip
 */
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import static java.lang.Math.random;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;

import org.modcs.tools.parser.model.ExecutionRuntime;
import org.modcs.tools.parser.model.Script;
import org.modcs.tools.parser.model.metrics.Metric;
import java.util.Map;
import javax.swing.JOptionPane;
//import calcula;

public class DOE_A1_Man_Corr_sla {
    private static Random random = new Random(System.currentTimeMillis());
    public static void main(String[] args) {

        try {
            
            
                //array para armazenar informações vindas do arquivo para o dispositivo UPS
                ArrayList ups = new ArrayList();

                //array para armazenar informações vindas do arquivo para o dispositivo SDT
                ArrayList transformer = new ArrayList();

                //array para armazenar informações vindas do arquivo para o dispositivo Subpainel
                ArrayList subpainel = new ArrayList();

                //array para armazenar informações vindas do arquivo para o dispositivo Powerstrip
                ArrayList powerstrip = new ArrayList();

                //array para armazenar informações de todas as disponibilidades do dispositivo UPS
                ArrayList upsDisp = new ArrayList();

                //array para armazenar informações de todas as disponibilidades do dispositivo UPS
                ArrayList transformerDisp = new ArrayList();

                //array para armazenar informações de todas as disponibilidades do dispositivo Subpainel
                ArrayList subpainelDisp = new ArrayList();

                //array para armazenar informações de todas as disponibilidades do dispositivo Powerstrip
                ArrayList powerstripDisp = new ArrayList();

                //instancia a class calculo que realizar o calculo com o mercury
                calculaMaCorrSla calculo = new calculaMaCorrSla(); 

                //array para armazenar informações das melhores disponibilidade do dispositivo UPS
                ArrayList upsDisponibilidade = new ArrayList();

                 //array para armazenar informações das melhores disponibilidade do dispositivo SDT 
                ArrayList transformerDisponibilidade = new ArrayList();     

                //array para armazenar informações das melhores disponibilidade do dispositivo Subpainel
                ArrayList subpainelDisponibilidade = new ArrayList();

                 //array para armazenar informações das melhores disponibilidade do dispositivo Powerstrip 
                ArrayList powerstripDisponibilidade = new ArrayList();    

                //carregar o arquivo com os dispositivos
                BufferedReader buffer = new BufferedReader(new FileReader("C://Users/felip/Desktop/Components270000g1.txt"));
                String linha = null;

                //Armazenar o melhor resultado
                double MelhorResultado = 0;
                //Armazenar o melhor caminho
                String MelhorCaminho = ""; 
            
                // Map utilizado para o dispositivo UPS
                Map<Double,String> dispositivo_UPS = new HashMap<Double,String>();
                Map<Double,String> Mttf_UPS = new HashMap<Double,String>();
                Map<Double,String> ID_UPS = new HashMap<Double,String>();
                //Map<Double,String> custo_UPS = new HashMap<Double,String>();

                // Map utilizado para o dispositivo SDT
                Map<Double,String> dispositivo_SDT = new HashMap<Double,String>();
                Map<Double,String> Mttf_SDT = new HashMap<Double,String>();
                Map<Double,String> ID_SDT = new HashMap<Double,String>();  
                //Map<Double,String> custo_SDT = new HashMap<Double,String>();

                // Map utilizado para o dispositivo Subpainel
                Map<Double,String> dispositivo_Subpainel = new HashMap<Double,String>();
                Map<Double,String> Mttf_Subpainel = new HashMap<Double,String>();
                Map<Double,String> ID_Subpainel = new HashMap<Double,String>();
               // Map<Double,String> custo_Subpainel = new HashMap<Double,String>();

                // Map utilizado para o dispositivo Powerstrip
                Map<Double,String> dispositivo_Powerstrip = new HashMap<Double,String>();
                Map<Double,String> Mttf_Powerstrip = new HashMap<Double,String>();
                Map<Double,String> ID_Powerstrip = new HashMap<Double,String>();
                //Map<Double,String> custo_Powerstrip = new HashMap<Double,String>();

                //ler o arquivo 
                while ((linha = buffer.readLine()) != null) {
                    String[] linhaSeparado = linha.split(";");
                    String equipamento = linhaSeparado[1];

                    //pegar as informações do dispositivo UPS
                    if (equipamento.equals("UPS_5KVA0")) {
                        ups.add(linhaSeparado);
                    }

                    //pegar as informações do dispositivo SDT
                    if (equipamento.equals("SDTransformer2")) {
                        transformer.add(linhaSeparado);
                    }

                    //pegar as informações do dispositivo Subpanel
                    if (equipamento.equals("Subpanel5")) {
                        subpainel.add(linhaSeparado);
                    }

                    //pegar as informações do dispositivo PowerStrip
                    if (equipamento.equals("PowerStrip6")) {
                        powerstrip.add(linhaSeparado);
                    }
                }

                //Percorrer o array com as informações do dispositivo UPS
                for (int u = 0; u < ups.size(); ++u) {
                    String[] equipamentoUps = (String[]) ups.get(u);    

                    //instancia a classe calculo camando o metodo que excutar o script do Mercury
                    double disponibilidade_UPS = calculo.executarScriptSimplificado(equipamentoUps[3], equipamentoUps[1]);

                    //adicionar as informações do MAP referente ao nome, id e mttf do dispositivo
                    upsDisp.add(disponibilidade_UPS);                 
                    dispositivo_UPS.put(disponibilidade_UPS, equipamentoUps[2]); 
                    Mttf_UPS.put(disponibilidade_UPS, equipamentoUps[3]);
                    ID_UPS.put(disponibilidade_UPS, equipamentoUps[0]);
                    //custo_UPS.put(disponibilidade_UPS, equipamentoUps[5]);


                }

                //Percorrer o array com as informações do dispositivo SDT
                for (int t = 0; t < transformer.size(); ++t) {
                    String[] equipamentoTransformer = (String[]) transformer.get(t);

                    //instancia a classe calculo camando o metodo que excutar o script do Mercury
                    double disponibilidade_SDT = calculo.executarScriptSimplificado(equipamentoTransformer[3], equipamentoTransformer[1]);

                    //adicionar as informações do MAP referente ao nome, id e mttf do dispositivo
                    transformerDisp.add(disponibilidade_SDT);                
                    dispositivo_SDT.put(disponibilidade_SDT, equipamentoTransformer[2]); 
                    Mttf_SDT.put(disponibilidade_SDT, equipamentoTransformer[3]);
                    ID_SDT.put(disponibilidade_SDT, equipamentoTransformer[0]);
                    //custo_SDT.put(disponibilidade_SDT, equipamentoTransformer[5]);

                }           

                //Percorrer o array com as informações do dispositivo Subpainel
                for (int t = 0; t < subpainel.size(); ++t) {
                    String[] equipamentoSubpainel = (String[]) subpainel.get(t);
                    //instancia a classe calculo camando o metodo que excutar o script do Mercury
                    double disponibilidade_Subpainel = calculo.executarScriptSimplificado(equipamentoSubpainel[3], equipamentoSubpainel[1]);

                    //adicionar as informações do MAP referente ao nome, id e mttf do dispositivo
                    subpainelDisp.add(disponibilidade_Subpainel);                
                    dispositivo_Subpainel.put(disponibilidade_Subpainel, equipamentoSubpainel[2]); 
                    Mttf_Subpainel.put(disponibilidade_Subpainel, equipamentoSubpainel[3]);
                    ID_Subpainel.put(disponibilidade_Subpainel, equipamentoSubpainel[0]);
                    //custo_Subpainel.put(disponibilidade_Subpainel, equipamentoSubpainel[5]);

                }

                 //Percorrer o array com as informações do dispositivo Powerstrip
                for (int t = 0; t < powerstrip.size(); ++t) {
                    String[] equipamentoPowerstrip = (String[]) powerstrip.get(t);

                    //instancia a classe calculo camando o metodo que excutar o script do Mercury
                    double disponibilidade_Powerstrip  = calculo.executarScriptSimplificado(equipamentoPowerstrip[3], equipamentoPowerstrip[1]);

                    //adicionar as informações do MAP referente ao nome, id e mttf do dispositivo
                    powerstripDisp.add(disponibilidade_Powerstrip);                
                    dispositivo_Powerstrip.put(disponibilidade_Powerstrip, equipamentoPowerstrip[2]); 
                    Mttf_Powerstrip.put(disponibilidade_Powerstrip, equipamentoPowerstrip[3]);
                    ID_Powerstrip.put(disponibilidade_Powerstrip, equipamentoPowerstrip[0]);
                   // custo_Powerstrip.put(disponibilidade_Powerstrip, equipamentoPowerstrip[5]);

                }

                int quantidade = Integer.parseInt(JOptionPane.showInputDialog("Digite a quantidade de pedidos aleatorios"));
                int sla = Integer.parseInt(JOptionPane.showInputDialog("Digite as horas do SLA"));
               //Atraves da collections ordena-se o array (decrescente) de disponibilidade para o dispositivo UPS 
               Collections.sort(upsDisp,Collections.reverseOrder());
               //System.out.println(upsDisp.get(0));
               //System.out.println(upsDisp.get(1));   

               //Adicionar no array de upsDisponibilidade as duas melhores disponibilidades
               upsDisponibilidade.add(upsDisp.get(0));
               upsDisponibilidade.add(upsDisp.get(1));               
                           
                boolean success = (new File("ManutencaoCorretivaSLA/2equipe/A1/"+sla)).mkdir();
                if (!success) {
                    // Falha no momento de criar o diretório
                     System.out.println("O arquivo não foi criado, talvez ele já exista");
                }
            
                BufferedWriter bw = new BufferedWriter(new FileWriter("ManutencaoCorretivaSLA/2equipe/A1/"+sla+"/ResultadosUPS_A1.txt", true));
                
                bw.write("UPS " + ID_UPS.get(upsDisp.get(0)) +  " = " + upsDisp.get(0) + "\n" + 
                         "UPS " + ID_UPS.get(upsDisp.get(1)) + " = " + upsDisp.get(1) + "\n");
                bw.close();

              //Atraves da collections ordena-se o array (decrescente) de disponibilidade para o dispositivo UPS 
               Collections.sort(transformerDisp,Collections.reverseOrder());

               //Adicionar no array de transformerDisponibilidade as duas melhores disponibilidades
               transformerDisponibilidade.add(transformerDisp.get(0));
               transformerDisponibilidade.add(transformerDisp.get(1));

                BufferedWriter sdt = new BufferedWriter(new FileWriter("ManutencaoCorretivaSLA/2equipe/A1/"+sla+"/ResultadosSDT_A1.txt", true));
                sdt.write("SDT " + ID_SDT.get(transformerDisp.get(0)) + " = " + transformerDisp.get(0) + "\n" + 
                          "SDT " + ID_SDT.get(transformerDisp.get(1)) + " = " + transformerDisp.get(1) + "\n" );
                sdt.close();            
                
                 
                //executar o experimento n vezes
                for (int w = 0; w < quantidade; ++w) {
                    // pega aleatoriamente um resultado
                    int SubPainel_aleatorio = random.nextInt(10);

                    //Adicionar no array de subpainelDisponibilidade as duas melhores disponibilidades
                    subpainelDisponibilidade.add(subpainelDisp.get(SubPainel_aleatorio));

                    BufferedWriter subPainel_equi = new BufferedWriter(new FileWriter("ManutencaoCorretivaSLA/2equipe/A1/"+sla+"/ResultadosSubPainel_A1.txt", true));
                    subPainel_equi.write("SubPainel " + ID_Subpainel.get(subpainelDisp.get(SubPainel_aleatorio)) + " = " + subpainelDisp.get(SubPainel_aleatorio) + "\n");
                    subPainel_equi.close();
                   
                    // pega aleatoriamente um resultado
                    int PowerStrip_aleatorio = random.nextInt(10);

                    //Adicionar no array de powerstripDisponibilidade as duas melhores disponibilidades
                    powerstripDisponibilidade.add(powerstripDisp.get(PowerStrip_aleatorio));

                    BufferedWriter powerstrip_equi = new BufferedWriter(new FileWriter("ManutencaoCorretivaSLA/2equipe/A1/"+sla+"/ResultadosPowerStrip.txt", true));
                    powerstrip_equi.write("Powerstrip " + ID_Powerstrip.get(powerstripDisp.get(PowerStrip_aleatorio)) + " = " + powerstripDisp.get(PowerStrip_aleatorio) + "\n");
                    powerstrip_equi.close();           

                    //for aninhado entre os array de disponibilidade de UPS e SDT
                    for (int up = 0; up < upsDisponibilidade.size(); ++up) {
                        for (int sd = 0; sd < transformerDisponibilidade.size(); ++sd) {                      

                            //Chama o metodo de calcular a disponibilidade                    
                            double resultado = calculo.executarScript(Mttf_UPS.get(upsDisponibilidade.get(up)),
                                                                      Mttf_SDT.get(transformerDisponibilidade.get(sd)), 
                                                                      Mttf_Subpainel.get(subpainelDisponibilidade.get(0)), 
                                                                      Mttf_Powerstrip.get(powerstripDisponibilidade.get(0)),
                                                                      dispositivo_UPS.get(upsDisponibilidade.get(up)),
                                                                      dispositivo_SDT.get(transformerDisponibilidade.get(sd)), 
                                                                      dispositivo_Subpainel.get(subpainelDisponibilidade.get(0)), 
                                                                      dispositivo_Powerstrip.get(powerstripDisponibilidade.get(0)),"1",120);
                            
                            double resultadoDuasEquipes = calculo.executarScript(Mttf_UPS.get(upsDisponibilidade.get(up)),
                                          Mttf_SDT.get(transformerDisponibilidade.get(sd)), 
                                          Mttf_Subpainel.get(subpainelDisponibilidade.get(0)), 
                                          Mttf_Powerstrip.get(powerstripDisponibilidade.get(0)),
                                          dispositivo_UPS.get(upsDisponibilidade.get(up)),
                                          dispositivo_SDT.get(transformerDisponibilidade.get(sd)), 
                                          dispositivo_Subpainel.get(subpainelDisponibilidade.get(0)), 
                                          dispositivo_Powerstrip.get(powerstripDisponibilidade.get(0)),"2",120);
                            
                            
                            double resultado48 = calculo.executarScript(Mttf_UPS.get(upsDisponibilidade.get(up)),
                                          Mttf_SDT.get(transformerDisponibilidade.get(sd)), 
                                          Mttf_Subpainel.get(subpainelDisponibilidade.get(0)), 
                                          Mttf_Powerstrip.get(powerstripDisponibilidade.get(0)),
                                          dispositivo_UPS.get(upsDisponibilidade.get(up)),
                                          dispositivo_SDT.get(transformerDisponibilidade.get(sd)), 
                                          dispositivo_Subpainel.get(subpainelDisponibilidade.get(0)), 
                                          dispositivo_Powerstrip.get(powerstripDisponibilidade.get(0)),"1",48);
                            
                            double resultadoDuasEquipes48 = calculo.executarScript(Mttf_UPS.get(upsDisponibilidade.get(up)),
                                          Mttf_SDT.get(transformerDisponibilidade.get(sd)), 
                                          Mttf_Subpainel.get(subpainelDisponibilidade.get(0)), 
                                          Mttf_Powerstrip.get(powerstripDisponibilidade.get(0)),
                                          dispositivo_UPS.get(upsDisponibilidade.get(up)),
                                          dispositivo_SDT.get(transformerDisponibilidade.get(sd)), 
                                          dispositivo_Subpainel.get(subpainelDisponibilidade.get(0)), 
                                          dispositivo_Powerstrip.get(powerstripDisponibilidade.get(0)),"2",48);
                            
                            
                            double resultado24 = calculo.executarScript(Mttf_UPS.get(upsDisponibilidade.get(up)),
                                          Mttf_SDT.get(transformerDisponibilidade.get(sd)), 
                                          Mttf_Subpainel.get(subpainelDisponibilidade.get(0)), 
                                          Mttf_Powerstrip.get(powerstripDisponibilidade.get(0)),
                                          dispositivo_UPS.get(upsDisponibilidade.get(up)),
                                          dispositivo_SDT.get(transformerDisponibilidade.get(sd)), 
                                          dispositivo_Subpainel.get(subpainelDisponibilidade.get(0)), 
                                          dispositivo_Powerstrip.get(powerstripDisponibilidade.get(0)),"1",24);

                            double resultadoDuasEquipes24 = calculo.executarScript(Mttf_UPS.get(upsDisponibilidade.get(up)),
                                          Mttf_SDT.get(transformerDisponibilidade.get(sd)), 
                                          Mttf_Subpainel.get(subpainelDisponibilidade.get(0)), 
                                          Mttf_Powerstrip.get(powerstripDisponibilidade.get(0)),
                                          dispositivo_UPS.get(upsDisponibilidade.get(up)),
                                          dispositivo_SDT.get(transformerDisponibilidade.get(sd)), 
                                          dispositivo_Subpainel.get(subpainelDisponibilidade.get(0)), 
                                          dispositivo_Powerstrip.get(powerstripDisponibilidade.get(0)),"2",24);
                            
                            double resultado8 = calculo.executarScript(Mttf_UPS.get(upsDisponibilidade.get(up)),
                                          Mttf_SDT.get(transformerDisponibilidade.get(sd)), 
                                          Mttf_Subpainel.get(subpainelDisponibilidade.get(0)), 
                                          Mttf_Powerstrip.get(powerstripDisponibilidade.get(0)),
                                          dispositivo_UPS.get(upsDisponibilidade.get(up)),
                                          dispositivo_SDT.get(transformerDisponibilidade.get(sd)), 
                                          dispositivo_Subpainel.get(subpainelDisponibilidade.get(0)), 
                                          dispositivo_Powerstrip.get(powerstripDisponibilidade.get(0)),"1",8);
                            
                            double resultadoDuasEquipes8 = calculo.executarScript(Mttf_UPS.get(upsDisponibilidade.get(up)),
                                          Mttf_SDT.get(transformerDisponibilidade.get(sd)), 
                                          Mttf_Subpainel.get(subpainelDisponibilidade.get(0)), 
                                          Mttf_Powerstrip.get(powerstripDisponibilidade.get(0)),
                                          dispositivo_UPS.get(upsDisponibilidade.get(up)),
                                          dispositivo_SDT.get(transformerDisponibilidade.get(sd)), 
                                          dispositivo_Subpainel.get(subpainelDisponibilidade.get(0)), 
                                          dispositivo_Powerstrip.get(powerstripDisponibilidade.get(0)),"2",8);

                         
                           /*Double custo = Double.valueOf(custo_UPS.get(upsDisponibilidade.get(up)))+ 
                                            Double.valueOf(custo_SDT.get(transformerDisponibilidade.get(sd))) +
                                            Double.valueOf(custo_Subpainel.get(subpainelDisponibilidade.get(0))) +
                                            Double.valueOf(custo_Powerstrip.get(powerstripDisponibilidade.get(0)));*/
                           
  
                           System.out.println("A1: UPS " + ID_UPS.get(upsDisponibilidade.get(up)) + 
                                               " - SDT " + ID_SDT.get(transformerDisponibilidade.get(sd)) + 
                                               " - Subpainel " + ID_Subpainel.get(subpainelDisponibilidade.get(0)) + 
                                               " - PowerStrip " + ID_Powerstrip.get(powerstripDisponibilidade.get(0)) + 
                                               //" - Custo " + custo +    
                                               " -- 120 = " + resultado + " -- " + resultadoDuasEquipes+ 
                                               " -- 48 = " + resultado48 + " -- " + resultadoDuasEquipes48 + 
                                               " -- 24 = " + resultado24 + " -- " + resultadoDuasEquipes24+ 
                                               " -- 8 = " + resultado8 + " -- " + resultadoDuasEquipes8);

                            // Escreve e fecha arquivo
                            BufferedWriter arquivo = new BufferedWriter(new FileWriter("ManutencaoCorretivaSLA/2equipe/A1/"+sla+"/ResultadosA1.txt", true));
                            arquivo.write("UPS " + ID_UPS.get(upsDisponibilidade.get(up)) + 
                            " - SDT " + ID_SDT.get(transformerDisponibilidade.get(sd)) +  
                            " - Subpainel " + ID_Subpainel.get(subpainelDisponibilidade.get(0)) + 
                            " - PowerStrip " + ID_Powerstrip.get(powerstripDisponibilidade.get(0)) + 
                            // " - Custo " + custo +         
                            " -- 120 = " + resultado + " -- " + resultadoDuasEquipes+ 
                                " -- 48 = " + resultado48 + " -- " + resultadoDuasEquipes48 + 
                                " -- 24 = " + resultado24 + " -- " + resultadoDuasEquipes24+ 
                                " -- 8 = " + resultado8 + " -- " + resultadoDuasEquipes8 + "\n");
                            arquivo.close();

                            if(MelhorResultado<resultado){
                                MelhorResultado = resultado;
                                MelhorCaminho = "UPS " + ID_UPS.get(upsDisponibilidade.get(up)) + 
                                                " - SDT " + ID_SDT.get(transformerDisponibilidade.get(sd)) +  
                                                " - Subpainel " + ID_Subpainel.get(subpainelDisponibilidade.get(0)) + 
                                                " - PowerStrip " + ID_Powerstrip.get(powerstripDisponibilidade.get(0)) + 
                                                " = " + resultado;
                            }               
                        }                
                    }

                    //demarcando fim do experimento
                    BufferedWriter arquivo = new BufferedWriter(new FileWriter("ManutencaoCorretivaSLA/2equipe/A1/"+sla+"/ResultadosA1.txt", true));
                    arquivo.write("\n");
                    arquivo.close();

                    //limpa o array de disponibilidades
                    subpainelDisponibilidade.clear();
                    powerstripDisponibilidade.clear();                   
                  
            }
                    //Imprimir o melhor resultado
                    System.out.println("O melhor resultado foi " + MelhorCaminho);
        } catch (IOException e) {
            System.out.print("Erro na abertura do arquivo: %s.\n");
        }

        System.out.println();
    }
    private String arq;
}

