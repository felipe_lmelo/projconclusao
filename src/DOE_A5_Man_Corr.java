import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import static java.lang.Math.random;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;

import org.modcs.tools.parser.model.ExecutionRuntime;
import org.modcs.tools.parser.model.Script;
import org.modcs.tools.parser.model.metrics.Metric;
import java.util.Map;
import javax.swing.JOptionPane;
//import calcula;

public class DOE_A5_Man_Corr {
    private static Random random = new Random(System.currentTimeMillis());
    public static void main(String[] args) {

        try {
            //array para armazenar informações vindas do arquivo para o dispositivo UPS
            ArrayList ups = new ArrayList();
            
            //array para armazenar informações vindas do arquivo para o dispositivo SDT
            ArrayList transformer = new ArrayList();
            
            //array para armazenar informações vindas do arquivo para o dispositivo Subpainel
            ArrayList subpainel = new ArrayList();
            
            //array para armazenar informações vindas do arquivo para o dispositivo Powerstrip
            ArrayList powerstrip = new ArrayList();
            
            //array para armazenar informações de todas as disponibilidades do dispositivo UPS
            ArrayList upsDisp = new ArrayList();
            
            //array para armazenar informações de todas as disponibilidades do dispositivo UPS
            ArrayList transformerDisp = new ArrayList();
            
            //array para armazenar informações de todas as disponibilidades do dispositivo Subpainel
            ArrayList subpainelDisp = new ArrayList();
            
            //array para armazenar informações de todas as disponibilidades do dispositivo Powerstrip
            ArrayList powerstripDisp = new ArrayList();
            
            //instancia a class calculo que realizar o calculo com o mercury
            calculaMaCorr calculo = new calculaMaCorr();
            
            //array para armazenar informações das melhores disponibilidade do dispositivo UPS
            ArrayList upsDisponibilidade = new ArrayList();
            
             //array para armazenar informações das melhores disponibilidade do dispositivo SDT 
            ArrayList transformerDisponibilidade = new ArrayList();     
            
            //array para armazenar informações das melhores disponibilidade do dispositivo Subpainel
            ArrayList subpainelDisponibilidade = new ArrayList();
            
             //array para armazenar informações das melhores disponibilidade do dispositivo Powerstrip 
            ArrayList powerstripDisponibilidade = new ArrayList();    
            
            //carregar o arquivo com os dispositivos
            BufferedReader buffer = new BufferedReader(new FileReader("C://Users/felip/Desktop/Components270000g1.txt"));
            String linha = null;
            
            //Armazenar o melhor resultado
            double MelhorResultado = 0;
            //Armazenar o melhor caminho
            String MelhorCaminho = ""; 
            
            // Map utilizado para o dispositivo UPS
            Map<Double,String> dispositivo_UPS = new HashMap<Double,String>();
            Map<Double,String> Mttf_UPS = new HashMap<Double,String>();
            Map<Double,String> ID_UPS = new HashMap<Double,String>();
            
            // Map utilizado para o dispositivo SDT
            Map<Double,String> dispositivo_SDT = new HashMap<Double,String>();
            Map<Double,String> Mttf_SDT = new HashMap<Double,String>();
            Map<Double,String> ID_SDT = new HashMap<Double,String>();  
            
            // Map utilizado para o dispositivo Subpainel
            Map<Double,String> dispositivo_Subpainel = new HashMap<Double,String>();
            Map<Double,String> Mttf_Subpainel = new HashMap<Double,String>();
            Map<Double,String> ID_Subpainel = new HashMap<Double,String>();
            
            // Map utilizado para o dispositivo Powerstrip
            Map<Double,String> dispositivo_Powerstrip = new HashMap<Double,String>();
            Map<Double,String> Mttf_Powerstrip = new HashMap<Double,String>();
            Map<Double,String> ID_Powerstrip = new HashMap<Double,String>();
            
            //ler o arquivo 
            while ((linha = buffer.readLine()) != null) {
                String[] linhaSeparado = linha.split(";");
                String equipamento = linhaSeparado[1];
                
                //pegar as informações do dispositivo UPS
                if (equipamento.equals("UPS_5KVA0")) {
                    ups.add(linhaSeparado);
                }
                
                //pegar as informações do dispositivo SDT
                if (equipamento.equals("SDTransformer2")) {
                    transformer.add(linhaSeparado);
                }
                
                //pegar as informações do dispositivo Subpanel
                if (equipamento.equals("Subpanel5")) {
                    subpainel.add(linhaSeparado);
                }
                
                //pegar as informações do dispositivo PowerStrip
                if (equipamento.equals("PowerStrip6")) {
                    powerstrip.add(linhaSeparado);
                }
            }
            
            //Percorrer o array com as informações do dispositivo UPS
            for (int u = 0; u < ups.size(); ++u) {
                String[] equipamentoUps = (String[]) ups.get(u);    
                
                //instancia a classe calculo camando o metodo que excutar o script do Mercury
                double disponibilidade_UPS = calculo.executarScriptSimplificado(equipamentoUps[3], equipamentoUps[1]);
                
                //adicionar as informações do MAP referente ao nome, id e mttf do dispositivo
                upsDisp.add(disponibilidade_UPS);                 
                dispositivo_UPS.put(disponibilidade_UPS, equipamentoUps[2]); 
                Mttf_UPS.put(disponibilidade_UPS, equipamentoUps[3]);
                ID_UPS.put(disponibilidade_UPS, equipamentoUps[0]);
                
            }
            
            //Percorrer o array com as informações do dispositivo SDT
            for (int t = 0; t < transformer.size(); ++t) {
                String[] equipamentoTransformer = (String[]) transformer.get(t);
                
                //instancia a classe calculo camando o metodo que excutar o script do Mercury
                double disponibilidade_SDT = calculo.executarScriptSimplificado(equipamentoTransformer[3], equipamentoTransformer[1]);
                
                //adicionar as informações do MAP referente ao nome, id e mttf do dispositivo
                transformerDisp.add(disponibilidade_SDT);                
                dispositivo_SDT.put(disponibilidade_SDT, equipamentoTransformer[2]); 
                Mttf_SDT.put(disponibilidade_SDT, equipamentoTransformer[3]);
                ID_SDT.put(disponibilidade_SDT, equipamentoTransformer[0]);
                
            }           
                        
            //Percorrer o array com as informações do dispositivo Subpainel
            for (int t = 0; t < subpainel.size(); ++t) {
                String[] equipamentoSubpainel = (String[]) subpainel.get(t);
                //instancia a classe calculo camando o metodo que excutar o script do Mercury
                double disponibilidade_Subpainel = calculo.executarScriptSimplificado(equipamentoSubpainel[3], equipamentoSubpainel[1]);
                
                //adicionar as informações do MAP referente ao nome, id e mttf do dispositivo
                subpainelDisp.add(disponibilidade_Subpainel);                
                dispositivo_Subpainel.put(disponibilidade_Subpainel, equipamentoSubpainel[2]); 
                Mttf_Subpainel.put(disponibilidade_Subpainel, equipamentoSubpainel[3]);
                ID_Subpainel.put(disponibilidade_Subpainel, equipamentoSubpainel[0]);
                
            }
            
             //Percorrer o array com as informações do dispositivo Powerstrip
            for (int t = 0; t < powerstrip.size(); ++t) {
                String[] equipamentoPowerstrip = (String[]) powerstrip.get(t);
                
                //instancia a classe calculo camando o metodo que excutar o script do Mercury
                double disponibilidade_Powerstrip  = calculo.executarScriptSimplificado(equipamentoPowerstrip[3], equipamentoPowerstrip[1]);
                
                //adicionar as informações do MAP referente ao nome, id e mttf do dispositivo
                powerstripDisp.add(disponibilidade_Powerstrip);                
                dispositivo_Powerstrip.put(disponibilidade_Powerstrip, equipamentoPowerstrip[2]); 
                Mttf_Powerstrip.put(disponibilidade_Powerstrip, equipamentoPowerstrip[3]);
                ID_Powerstrip.put(disponibilidade_Powerstrip, equipamentoPowerstrip[0]);
                
            }
            
           //Atraves da collections ordena-se o array (decrescente) de disponibilidade para o dispositivo UPS 
           Collections.sort(upsDisp,Collections.reverseOrder());
           //System.out.println(upsDisp.get(0));
           //System.out.println(upsDisp.get(1));   
           
           //Adicionar no array de upsDisponibilidade as duas melhores disponibilidades
           upsDisponibilidade.add(upsDisp.get(0));
           upsDisponibilidade.add(upsDisp.get(1));
                   
            BufferedWriter bw = new BufferedWriter(new FileWriter("ManutencaoCorretiva/2equipe/A5/ResultadosUPS_A5.txt", true));
            bw.write("UPS " + ID_UPS.get(upsDisp.get(0)) +  " = " + upsDisp.get(0) + "\n" + 
                     "UPS " + ID_UPS.get(upsDisp.get(1)) + " = " + upsDisp.get(1) + "\n");
            bw.close();
  
            
          //Atraves da collections ordena-se o array (decrescente) de disponibilidade para o dispositivo UPS 
           Collections.sort(transformerDisp,Collections.reverseOrder());
           
           //Adicionar no array de transformerDisponibilidade as duas melhores disponibilidades
           transformerDisponibilidade.add(transformerDisp.get(0));
           transformerDisponibilidade.add(transformerDisp.get(1));
           
            BufferedWriter sdt = new BufferedWriter(new FileWriter("ManutencaoCorretiva/2equipe/A5/ResultadosSDT_A5.txt", true));
            sdt.write("SDT " + ID_SDT.get(transformerDisp.get(0)) + " = " + transformerDisp.get(0) + "\n" + 
                      "SDT " + ID_SDT.get(transformerDisp.get(1)) + " = " + transformerDisp.get(1) + "\n" );
            sdt.close();
                        
             int quantidade = Integer.parseInt(JOptionPane.showInputDialog("Digite a quantidade de pedidos aleatorios"));
            // pega aleatoriamente um resultado
            for (int w = 0; w < quantidade; ++w) {
                int SubPainel_aleatorio = random.nextInt(10);

                //Adicionar no array de subpainelDisponibilidade as duas melhores disponibilidades
                subpainelDisponibilidade.add(subpainelDisp.get(SubPainel_aleatorio));

                BufferedWriter subPainel_equi = new BufferedWriter(new FileWriter("ManutencaoCorretiva/2equipe/A5/ResultadosSubPainel_A5.txt", true));
                subPainel_equi.write("SubPainel " + ID_Subpainel.get(subpainelDisp.get(SubPainel_aleatorio)) + " = " + subpainelDisp.get(SubPainel_aleatorio) + "\n");
                subPainel_equi.close();

                // pega aleatoriamente um resultado
                int PowerStrip_aleatorio = random.nextInt(10);

                //Adicionar no array de powerstripDisponibilidade as duas melhores disponibilidades
                powerstripDisponibilidade.add(powerstripDisp.get(PowerStrip_aleatorio));

                BufferedWriter powerstrip_equi = new BufferedWriter(new FileWriter("ManutencaoCorretiva/2equipe/A5/ResultadosPowerStrip_A5.txt", true));
                powerstrip_equi.write("Powerstrip " + ID_Powerstrip.get(powerstripDisp.get(PowerStrip_aleatorio)) + " = " + powerstripDisp.get(PowerStrip_aleatorio) + "\n");
                powerstrip_equi.close();           

                //for aninhado entre os array de disponibilidade de UPS e SDT
                for (int up = 0; up < upsDisponibilidade.size(); ++up) {
                    for (int sd = 0; sd < transformerDisponibilidade.size(); ++sd) {                      

                        //Chama o metodo de calcular a disponibilidade A5
                        double resultadoA5 = calculo.executarScriptA5(Mttf_UPS.get(upsDisponibilidade.get(up)),
                                                                      Mttf_SDT.get(transformerDisponibilidade.get(sd)), 
                                                                      Mttf_Subpainel.get(subpainelDisponibilidade.get(0)), 
                                                                      Mttf_Powerstrip.get(powerstripDisponibilidade.get(0)),
                                                                      "24038462",
                                                                      dispositivo_UPS.get(upsDisponibilidade.get(up)),
                                                                      dispositivo_SDT.get(transformerDisponibilidade.get(sd)),
                                                                      dispositivo_Subpainel.get(subpainelDisponibilidade.get(0)), 
                                                                      dispositivo_Powerstrip.get(powerstripDisponibilidade.get(0)),
                                                                      "STS","1");
                        
                        
                        double resultadoDuasEquipes = calculo.executarScriptA5(Mttf_UPS.get(upsDisponibilidade.get(up)),
                                              Mttf_SDT.get(transformerDisponibilidade.get(sd)), 
                                              Mttf_Subpainel.get(subpainelDisponibilidade.get(0)), 
                                              Mttf_Powerstrip.get(powerstripDisponibilidade.get(0)),
                                              "24038462",
                                              dispositivo_UPS.get(upsDisponibilidade.get(up)),
                                              dispositivo_SDT.get(transformerDisponibilidade.get(sd)),
                                              dispositivo_Subpainel.get(subpainelDisponibilidade.get(0)), 
                                              dispositivo_Powerstrip.get(powerstripDisponibilidade.get(0)),
                                              "STS","2");

                     /*   System.out.println("A5: UPS " + ID_UPS.get(upsDisponibilidade.get(up)) + 
                                            " - UPS " + ID_UPS.get(upsDisponibilidade.get(up)) +  
                                            " - SDT " + ID_SDT.get(transformerDisponibilidade.get(sd)) +
                                            " - SDT " + ID_SDT.get(transformerDisponibilidade.get(sd)) + 
                                            " - Subpainel " + ID_Subpainel.get(subpainelDisponibilidade.get(0)) + 
                                            " - Subpainel " + ID_Subpainel.get(subpainelDisponibilidade.get(0)) + 
                                            " - PowerStrip " + ID_Powerstrip.get(powerstripDisponibilidade.get(0)) + 
                                            " - PowerStrip " + ID_Powerstrip.get(powerstripDisponibilidade.get(0)) + " = " + resultadoA5); */

                        // Escreve e fecha arquivo
                        BufferedWriter arquivoA5 = new BufferedWriter(new FileWriter("ManutencaoCorretiva/2equipe/A5/ResultadosA5.txt", true));
                        arquivoA5.write("UPS " + ID_UPS.get(upsDisponibilidade.get(up)) + 
                                     " - UPS " + ID_UPS.get(upsDisponibilidade.get(up)) + 
                                     " - SDT " + ID_SDT.get(transformerDisponibilidade.get(sd)) + 
                                     " - SDT " + ID_SDT.get(transformerDisponibilidade.get(sd)) +  
                                     " - Subpainel " + ID_Subpainel.get(subpainelDisponibilidade.get(0)) +  
                                     " - Subpainel " + ID_Subpainel.get(subpainelDisponibilidade.get(0)) + 
                                     " - PowerStrip " + ID_Powerstrip.get(powerstripDisponibilidade.get(0)) + 
                                     " - PowerStrip " + ID_Powerstrip.get(powerstripDisponibilidade.get(0)) + 
                                     " = " + resultadoA5 + " --- " + resultadoDuasEquipes  +"\n");
                        arquivoA5.close();
                        
                         if(MelhorResultado<resultadoA5){
                                MelhorResultado = resultadoA5;
                                MelhorCaminho = "UPS " + ID_UPS.get(upsDisponibilidade.get(up)) + 
                                                " - UPS " + ID_UPS.get(upsDisponibilidade.get(up)) + 
                                                " - SDT " + ID_SDT.get(transformerDisponibilidade.get(sd)) + 
                                                " - SDT " + ID_SDT.get(transformerDisponibilidade.get(sd)) +  
                                                " - Subpainel " + ID_Subpainel.get(subpainelDisponibilidade.get(0)) +  
                                                " - Subpainel " + ID_Subpainel.get(subpainelDisponibilidade.get(0)) + 
                                                " - PowerStrip " + ID_Powerstrip.get(powerstripDisponibilidade.get(0)) + 
                                                " - PowerStrip " + ID_Powerstrip.get(powerstripDisponibilidade.get(0)) + " = " + resultadoA5;
                            }   

                    }                
                }     
                //demarcando fim do experimento
                    BufferedWriter arquivo = new BufferedWriter(new FileWriter("ManutencaoCorretiva/2equipe/A5/ResultadosA5.txt", true));
                    arquivo.write("\n");
                    arquivo.close();

                //limpa o array de disponibilidades
                subpainelDisponibilidade.clear();
                powerstripDisponibilidade.clear();
                    
            }
        //Imprimir o melhor resultado
        System.out.println("O melhor resultado foi " + MelhorCaminho);            
        } catch (IOException e) {
            System.out.print("Erro na abertura do arquivo: %s.\n");
        }

        System.out.println();
    }
    private String arq;
}
