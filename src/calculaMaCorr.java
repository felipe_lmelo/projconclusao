
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import org.modcs.tools.parser.model.ExecutionRuntime;
import org.modcs.tools.parser.model.Script;
import org.modcs.tools.parser.model.metrics.Metric;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author fmelo
 */
public class calculaMaCorr {
        
    static String script(String equipamentoUps, String equipamentoTransformer, String equipamentoSubpainel, String equipamentoPowerstrip, String ups, String sdt, String subpainel, String powerstrip, String qtdEquipe) {

        //System.out.println(equipamentoUps + " ----" + equipamentoTransformer + " ----" + equipamentoSubpainel+ " ----" + equipamentoPowerstrip);
        //System.exit(0);
        return "SPN Model{\n" +
                "\n" +
                "place POWE_OFF;\n" +
                "place POWE_ON( tokens= 1 );\n" +
                "place REPARAR_POWE;\n" +
                "place REPARAR_SDT;\n" +
                "place REPARAR_SUB;\n" +
                "place REPARAR_UPS;\n" +
                "place SDT_OFF;\n" +
                "place SDT_ON( tokens= 1 );\n" +
                "place SUB_OFF;\n" +
                "place SUB_ON( tokens= 1 );\n" +
                "place UPS_OFF( tokens= 0 );\n" +
                "place UPS_ON( tokens= 1 );\n" +
                "place equipeManutencao( tokens= "+ qtdEquipe +" );\n" +
                "place inicial( tokens= 1 );\n" +
                "place primeiroAno;\n" +
                "place quartoAno;\n" +
                "place quintoAno;\n" +
                "place segundoAno;\n" +
                "place terceiroAno;\n" +
                "\n" +
                "\n" +
                "immediateTransition acionarManutencaoPOWE(\n" +
                "inputs = [POWE_OFF, equipeManutencao],\n" +
                "outputs = [REPARAR_POWE]\n" +
                ");\n" +
                "\n" +
                "immediateTransition acionarManutencaoSDT(\n" +
                "inputs = [SDT_OFF, equipeManutencao],\n" +
                "outputs = [REPARAR_SDT]\n" +
                ");\n" +
                "\n" +
                "immediateTransition acionarManutencaoSUB(\n" +
                "inputs = [SUB_OFF, equipeManutencao],\n" +
                "outputs = [REPARAR_SUB]\n" +
                ");\n" +
                "\n" +
                "immediateTransition acionarManutencaoUPS(\n" +
                "inputs = [UPS_OFF, equipeManutencao],\n" +
                "outputs = [REPARAR_UPS]\n" +
                ");\n" +
                "\n" +
                "timedTransition MTTF_POWE(\n" +
                "inputs = [POWE_ON],\n" +
                "outputs = [POWE_OFF],\n" +
                "delay = \""+equipamentoPowerstrip+"-("+equipamentoPowerstrip+"*0.1*#primeiroAno)-("+equipamentoPowerstrip+"*0.2*#segundoAno)-("+equipamentoPowerstrip+"*0.3*#terceiroAno)-("+equipamentoPowerstrip+"*0.4*#quartoAno)-("+equipamentoPowerstrip+"*0.5*#quintoAno)\"\n" +
                ");\n" +
                "\n" +
                "timedTransition MTTF_SDT(\n" +
                "inputs = [SDT_ON],\n" +
                "outputs = [SDT_OFF],\n" +
                "delay = \""+equipamentoTransformer+"-("+equipamentoTransformer+"*0.1*#primeiroAno)-("+equipamentoTransformer+"*0.2*#segundoAno)-("+equipamentoTransformer+"*0.3*#terceiroAno)-("+equipamentoTransformer+"*0.4*#quartoAno)-("+equipamentoTransformer+"*0.5*#quintoAno)\"\n" +
                ");\n" +
                "\n" +
                "timedTransition MTTF_SUB(\n" +
                "inputs = [SUB_ON],\n" +
                "outputs = [SUB_OFF],\n" +
                "delay = \""+equipamentoSubpainel+"-("+equipamentoSubpainel+"*0.1*#primeiroAno)-("+equipamentoSubpainel+"*0.2*#segundoAno)-("+equipamentoSubpainel+"*0.3*#terceiroAno)-("+equipamentoSubpainel+"*0.4*#quartoAno)-("+equipamentoSubpainel+"*0.5*#quintoAno)\"\n" +
                ");\n" +
                "\n" +
                "timedTransition MTTF_UPS(\n" +
                "inputs = [UPS_ON],\n" +
                "outputs = [UPS_OFF],\n" +
                "delay = \""+equipamentoUps+"-("+equipamentoUps+"*0.1*#primeiroAno)-("+equipamentoUps+"*0.2*#segundoAno)-("+equipamentoUps+"*0.3*#terceiroAno)-("+equipamentoUps+"*0.4*#quartoAno)-("+equipamentoUps+"*0.5*#quintoAno)\"\n" +
                ");\n" +
                "\n" +
                "timedTransition MTTR_POWE(\n" +
                "inputs = [REPARAR_POWE],\n" +
                "outputs = [POWE_ON, equipeManutencao],\n" +
                "delay = 8.0\n" +
                ");\n" +
                "\n" +
                "timedTransition MTTR_SDT(\n" +
                "inputs = [REPARAR_SDT],\n" +
                "outputs = [SDT_ON, equipeManutencao],\n" +
                "delay = 8.0\n" +
                ");\n" +
                "\n" +
                "timedTransition MTTR_SUB(\n" +
                "inputs = [REPARAR_SUB],\n" +
                "outputs = [SUB_ON, equipeManutencao],\n" +
                "delay = 8.0\n" +
                ");\n" +
                "\n" +
                "timedTransition MTTR_UPS(\n" +
                "inputs = [REPARAR_UPS],\n" +
                "outputs = [UPS_ON, equipeManutencao],\n" +
                "delay = 8.0\n" +
                ");\n" +
                "\n" +
                "timedTransition clockPrimeiroAno(\n" +
                "inputs = [inicial],\n" +
                "outputs = [primeiroAno],\n" +
                "delay = 8760.0\n" +
                ");\n" +
                "\n" +
                "timedTransition clockQuartoAno(\n" +
                "inputs = [terceiroAno],\n" +
                "outputs = [quartoAno],\n" +
                "delay = 8760.0\n" +
                ");\n" +
                "\n" +
                "timedTransition clockQuintoAno(\n" +
                "inputs = [quartoAno],\n" +
                "outputs = [quintoAno],\n" +
                "delay = 8760.0\n" +
                ");\n" +
                "\n" +
                "timedTransition clockSegundoAno(\n" +
                "inputs = [primeiroAno],\n" +
                "outputs = [segundoAno],\n" +
                "delay = 8760.0\n" +
                ");\n" +
                "\n" +
                "timedTransition clockTerceiroAno(\n" +
                "inputs = [segundoAno],\n" +
                "outputs = [terceiroAno],\n" +
                "delay = 8760.0\n" +
                ");\n" +
                "\n" +
                "metric av = stationaryAnalysis( expression = \"P{((#UPS_ON=1)AND(#SDT_ON=1)AND(#SUB_ON=1)AND(#POWE_ON=1))}\" );\n" +
                "}\n" +
                "\n" +
                "main {\n" +
                "av = solve( Model,av );\n" +
                "println(av);\n" +
                "\n" +
                "}";
    }
    
    static double executarScript(String equipamentoUps, String equipamentoTransformer, String equipamentoSubpainel, String equipamentoPowerstrip, String ups, String sdt, String subpainel, String powerstrip, String qtdEquipe) throws IOException {
       
        ExecutionRuntime runtime = new ExecutionRuntime();
        //System.out.println(script(equipamentoUps, equipamentoTransformer, equipamentoSubpainel, equipamentoPowerstrip, ups, sdt, subpainel, powerstrip));
        //System.exit(0);
        Script script;
        script = new Script(script(equipamentoUps, equipamentoTransformer, equipamentoSubpainel, equipamentoPowerstrip, ups, sdt, subpainel, powerstrip,qtdEquipe));
        runtime.evaluateScript(script);
        
        

        double av;
        Metric Aval;

        Aval = runtime.getModel("Model").getMetric("av");
        av = Aval.solve();
        
        //escrever no arquivo
        //BufferedWriter bw = new BufferedWriter(new FileWriter("teste.txt", true));
        
        // Escreve e fecha arquivo
        //bw.write(av + "\t");
        //bw.close();
        return av;
    }
    
    static String scriptSimplificado(String equipamentoUps, String ups) {

        return "SPN Model{ \n"
                + "place " + ups + "_on( tokens= 1 ); \n"
                + "place " + ups + "_off; \n"
                + "timedTransition failure" + ups.substring(0, 1).toUpperCase() + ups.substring(1) + "(\n"
                + "inputs = [" + ups + "_on],\n"
                + "outputs = [" + ups + "_off],\n"
                + "delay = " + equipamentoUps + ");\n"
                + "timedTransition repair" + ups.substring(0, 1).toUpperCase() + ups.substring(1) + "(\n"
                + "inputs = [" + ups + "_off],\n"
                + "outputs = [" + ups + "_on],\n"
                + "delay = 8.0);\n"
                + "metric av = stationaryAnalysis( expression = \"P{(#" + ups + "_on=1)}\");}\n"
                + "main {\n"
                + "av = solve( Model,av );\n"
                + "println(av);}\n";
    }
    
      static double executarScriptSimplificado(String equipamentoUps, String ups) throws IOException {
       
        ExecutionRuntime runtime = new ExecutionRuntime();
        //System.out.println(script(equipamentoUps, ups));
        Script script = new Script(scriptSimplificado(equipamentoUps, ups));
        runtime.evaluateScript(script);

        double av;
        Metric Aval;

        Aval = runtime.getModel("Model").getMetric("av");
        av = Aval.solve();
        
        //escrever no arquivo
       // BufferedWriter bw = new BufferedWriter(new FileWriter("teste.txt", true));
        
        // Escreve e fecha arquivo
        //bw.write(av + "\t");
       // bw.close();
        return av;
    }
      
    static String scriptA2(String equipamentoUps, String equipamentoTransformer, String equipamentoSubpainel, String equipamentoPowerstrip, String equipamentoSts, String ups, String sdt, String subpainel, String powerstrip, String sts,String qtdEquipe) {

        return "SPN Model{\n" +
                "\n" +
                "place POWE_OFF;\n" +
                "place POWE_ON( tokens= 1 );\n" +
                "place REPARAR_POWE;\n" +
                "place REPARAR_SDT;\n" +
                "place REPARAR_STS;\n" +
                "place REPARAR_SUB;\n" +
                "place REPARAR_UPS;\n" +
                "place REPARAR_UPS2;\n" +
                "place SDT_OFF;\n" +
                "place SDT_ON( tokens= 1 );\n" +
                "place STS_OFF;\n" +
                "place STS_ON( tokens= 1 );\n" +
                "place SUB_OFF;\n" +
                "place SUB_ON( tokens= 1 );\n" +
                "place UPS_OFF;\n" +
                "place UPS_OFF2;\n" +
                "place UPS_ON( tokens= 1 );\n" +
                "place UPS_ON2( tokens= 1 );\n" +
                "place equipeManutencao( tokens= "+ qtdEquipe +" );\n" +
                "place inicial( tokens= 1 );\n" +
                "place primeiroAno;\n" +
                "place quartoAno;\n" +
                "place quintoAno;\n" +
                "place segundoAno;\n" +
                "place terceiroAno;\n" +
                "\n" +
                "\n" +
                "immediateTransition acionarManutencaoPOWE(\n" +
                "inputs = [POWE_OFF, equipeManutencao],\n" +
                "outputs = [REPARAR_POWE]\n" +
                ");\n" +
                "\n" +
                "immediateTransition acionarManutencaoSDT(\n" +
                "inputs = [SDT_OFF, equipeManutencao],\n" +
                "outputs = [REPARAR_SDT]\n" +
                ");\n" +
                "\n" +
                "immediateTransition acionarManutencaoSTS(\n" +
                "inputs = [STS_OFF, equipeManutencao],\n" +
                "outputs = [REPARAR_STS]\n" +
                ");\n" +
                "\n" +
                "immediateTransition acionarManutencaoSUB(\n" +
                "inputs = [SUB_OFF, equipeManutencao],\n" +
                "outputs = [REPARAR_SUB]\n" +
                ");\n" +
                "\n" +
                "immediateTransition acionarManutencaoUPS(\n" +
                "inputs = [UPS_OFF, equipeManutencao],\n" +
                "outputs = [REPARAR_UPS]\n" +
                ");\n" +
                "\n" +
                "immediateTransition acionarManutencaoUPS2(\n" +
                "inputs = [UPS_OFF2, equipeManutencao],\n" +
                "outputs = [REPARAR_UPS2]\n" +
                ");\n" +
                "\n" +
                "timedTransition MTTF_POWE(\n" +
                "inputs = [POWE_ON],\n" +
                "outputs = [POWE_OFF],\n" +
                "delay = \""+equipamentoPowerstrip+"-("+equipamentoPowerstrip+"*0.1*#primeiroAno)-("+equipamentoPowerstrip+"*0.2*#segundoAno)-("+equipamentoPowerstrip+"*0.3*#terceiroAno)-("+equipamentoPowerstrip+"*0.4*#quartoAno)-("+equipamentoPowerstrip+"*0.5*#quintoAno)\"\n" +
                ");\n" +
                "\n" +
                "timedTransition MTTF_SDT(\n" +
                "inputs = [SDT_ON],\n" +
                "outputs = [SDT_OFF],\n" +
                "delay = \""+equipamentoTransformer+"-("+equipamentoTransformer+"*0.1*#primeiroAno)-("+equipamentoTransformer+"*0.2*#segundoAno)-("+equipamentoTransformer+"*0.3*#terceiroAno)-("+equipamentoTransformer+"*0.4*#quartoAno)-("+equipamentoTransformer+"*0.5*#quintoAno)\"\n" +
                ");\n" +
                "\n" +
                "timedTransition MTTF_STS(\n" +
                "inputs = [STS_ON],\n" +
                "outputs = [STS_OFF],\n" +
                "delay = \""+equipamentoSts+"-("+equipamentoSts+"*0.1*#primeiroAno)-("+equipamentoSts+"*0.2*#segundoAno)-("+equipamentoSts+"*0.3*#terceiroAno)-("+equipamentoSts+"*0.4*#quartoAno)-("+equipamentoSts+"*0.5*#quintoAno)\"\n" +
                ");\n" +
                "\n" +
                "timedTransition MTTF_SUB(\n" +
                "inputs = [SUB_ON],\n" +
                "outputs = [SUB_OFF],\n" +
                "delay = \""+equipamentoSubpainel+"-("+equipamentoSubpainel+"*0.1*#primeiroAno)-("+equipamentoSubpainel+"*0.2*#segundoAno)-("+equipamentoSubpainel+"*0.3*#terceiroAno)-("+equipamentoSubpainel+"*0.4*#quartoAno)-("+equipamentoSubpainel+"*0.5*#quintoAno)\"\n" +
                ");\n" +
                "\n" +
                "timedTransition MTTF_UPS(\n" +
                "inputs = [UPS_ON],\n" +
                "outputs = [UPS_OFF],\n" +
                "delay = \""+equipamentoUps+"-("+equipamentoUps+"*0.1*#primeiroAno)-("+equipamentoUps+"*0.2*#segundoAno)-("+equipamentoUps+"*0.3*#terceiroAno)-("+equipamentoUps+"*0.4*#quartoAno)-("+equipamentoUps+"*0.5*#quintoAno)\"\n" +
                ");\n" +
                "\n" +
                "timedTransition MTTF_UPS2(\n" +
                "inputs = [UPS_ON2],\n" +
                "outputs = [UPS_OFF2],\n" +
                "delay = \""+equipamentoUps+"-("+equipamentoUps+"*0.1*#primeiroAno)-("+equipamentoUps+"*0.2*#segundoAno)-("+equipamentoUps+"*0.3*#terceiroAno)-("+equipamentoUps+"*0.4*#quartoAno)-("+equipamentoUps+"*0.5*#quintoAno)\"\n" +
                ");\n" +
                "\n" +
                "timedTransition MTTR_POWE(\n" +
                "inputs = [REPARAR_POWE],\n" +
                "outputs = [POWE_ON, equipeManutencao],\n" +
                "delay = 8.0\n" +
                ");\n" +
                "\n" +
                "timedTransition MTTR_SDT(\n" +
                "inputs = [REPARAR_SDT],\n" +
                "outputs = [SDT_ON, equipeManutencao],\n" +
                "delay = 8.0\n" +
                ");\n" +
                "\n" +
                "timedTransition MTTR_STS(\n" +
                "inputs = [REPARAR_STS],\n" +
                "outputs = [STS_ON, equipeManutencao],\n" +
                "delay = 8.0\n" +
                ");\n" +
                "\n" +
                "timedTransition MTTR_SUB(\n" +
                "inputs = [REPARAR_SUB],\n" +
                "outputs = [SUB_ON, equipeManutencao],\n" +
                "delay = 8.0\n" +
                ");\n" +
                "\n" +
                "timedTransition MTTR_UPS(\n" +
                "inputs = [REPARAR_UPS],\n" +
                "outputs = [UPS_ON, equipeManutencao],\n" +
                "delay = 8.0\n" +
                ");\n" +
                "\n" +
                "timedTransition MTTR_UPS2(\n" +
                "inputs = [REPARAR_UPS2],\n" +
                "outputs = [UPS_ON2, equipeManutencao],\n" +
                "delay = 8.0\n" +
                ");\n" +
                "\n" +
                "timedTransition clockPrimeiroAno(\n" +
                "inputs = [inicial],\n" +
                "outputs = [primeiroAno],\n" +
                "delay = 8760.0\n" +
                ");\n" +
                "\n" +
                "timedTransition clockQuartoAno(\n" +
                "inputs = [terceiroAno],\n" +
                "outputs = [quartoAno],\n" +
                "delay = 8760.0\n" +
                ");\n" +
                "\n" +
                "timedTransition clockQuintoAno(\n" +
                "inputs = [quartoAno],\n" +
                "outputs = [quintoAno],\n" +
                "delay = 8760.0\n" +
                ");\n" +
                "\n" +
                "timedTransition clockSegundoAno(\n" +
                "inputs = [primeiroAno],\n" +
                "outputs = [segundoAno],\n" +
                "delay = 8760.0\n" +
                ");\n" +
                "\n" +
                "timedTransition clockTerceiroAno(\n" +
                "inputs = [segundoAno],\n" +
                "outputs = [terceiroAno],\n" +
                "delay = 8760.0\n" +
                ");\n" +
                "\n" +
                "metric av = stationaryAnalysis( expression = \"P{((#UPS_ON=1)OR(#UPS_ON2=1))AND(#STS_ON=1)AND(#SDT_ON=1)AND(#SUB_ON=1)AND(#POWE_ON=1)}\" );\n" +
                "}\n" +
                "\n" +
                "main {\n" +
                "av = solve( Model,av );\n" +
                "println(av);\n" +
                "\n" +
                "}";
    }
    
    static double executarScriptA2(String equipamentoUps, String equipamentoTransformer, String equipamentoSubpainel, String equipamentoPowerstrip, String equipamentoSts, String ups, String sdt, String subpainel, String powerstrip, String sts, String qtdEquipe) throws IOException {
       
        ExecutionRuntime runtime = new ExecutionRuntime();
        //System.out.println(script(equipamentoUps, ups));
        Script script;
        script = new Script(scriptA2(equipamentoUps, equipamentoTransformer, equipamentoSubpainel, equipamentoPowerstrip, equipamentoSts, ups, sdt, subpainel, powerstrip, sts,qtdEquipe));
        //System.out.println(script);
        //System.exit(0);
        runtime.evaluateScript(script);

        double av;
        Metric Aval;

        Aval = runtime.getModel("Model").getMetric("av");
        av = Aval.solve();
        
        //escrever no arquivo
        //BufferedWriter bw = new BufferedWriter(new FileWriter("teste.txt", true));
        
        // Escreve e fecha arquivo
        //bw.write(av + "\t");
        //bw.close();
        return av;
    }
        
    static String scriptA3(String equipamentoUps, String equipamentoTransformer, String equipamentoSubpainel, String equipamentoPowerstrip, String equipamentoSts, String ups, String sdt, String subpainel, String powerstrip, String sts,String qtdEquipe) {

        return "SPN Model{\n" +
                "\n" +
                "place POWE_OFF;\n" +
                "place POWE_ON( tokens= 1 );\n" +
                "place REPARAR_POWE;\n" +
                "place REPARAR_SDT;\n" +
                "place REPARAR_SDT2;\n" +
                "place REPARAR_STS;\n" +
                "place REPARAR_SUB;\n" +
                "place REPARAR_UPS;\n" +
                "place REPARAR_UPS2;\n" +
                "place SDT_OFF;\n" +
                "place SDT_OFF2;\n" +
                "place SDT_ON( tokens= 1 );\n" +
                "place SDT_ON2( tokens= 1 );\n" +
                "place STS_OFF;\n" +
                "place STS_ON( tokens= 1 );\n" +
                "place SUB_OFF;\n" +
                "place SUB_ON( tokens= 1 );\n" +
                "place UPS_OFF;\n" +
                "place UPS_OFF2;\n" +
                "place UPS_ON( tokens= 1 );\n" +
                "place UPS_ON2( tokens= 1 );\n" +
                "place equipeManutencao( tokens= "+ qtdEquipe +" );\n" +
                "place inicial( tokens= 1 );\n" +
                "place primeiroAno;\n" +
                "place quartoAno;\n" +
                "place quintoAno;\n" +
                "place segundoAno;\n" +
                "place terceiroAno;\n" +
                "\n" +
                "\n" +
                "immediateTransition acionarManutencaoPOWE(\n" +
                "inputs = [POWE_OFF, equipeManutencao],\n" +
                "outputs = [REPARAR_POWE]\n" +
                ");\n" +
                "\n" +
                "immediateTransition acionarManutencaoSDT(\n" +
                "inputs = [SDT_OFF, equipeManutencao],\n" +
                "outputs = [REPARAR_SDT]\n" +
                ");\n" +
                "\n" +
                "immediateTransition acionarManutencaoSDT2(\n" +
                "inputs = [SDT_OFF2, equipeManutencao],\n" +
                "outputs = [REPARAR_SDT2]\n" +
                ");\n" +
                "\n" +
                "immediateTransition acionarManutencaoSTS(\n" +
                "inputs = [STS_OFF, equipeManutencao],\n" +
                "outputs = [REPARAR_STS]\n" +
                ");\n" +
                "\n" +
                "immediateTransition acionarManutencaoSUB(\n" +
                "inputs = [SUB_OFF, equipeManutencao],\n" +
                "outputs = [REPARAR_SUB]\n" +
                ");\n" +
                "\n" +
                "immediateTransition acionarManutencaoUPS(\n" +
                "inputs = [UPS_OFF, equipeManutencao],\n" +
                "outputs = [REPARAR_UPS]\n" +
                ");\n" +
                "\n" +
                "immediateTransition acionarManutencaoUPS2(\n" +
                "inputs = [UPS_OFF2, equipeManutencao],\n" +
                "outputs = [REPARAR_UPS2]\n" +
                ");\n" +
                "\n" +
                "timedTransition MTTF_POWE(\n" +
                "inputs = [POWE_ON],\n" +
                "outputs = [POWE_OFF],\n" +
                "delay = \""+equipamentoPowerstrip+"-("+equipamentoPowerstrip+"*0.1*#primeiroAno)-("+equipamentoPowerstrip+"*0.2*#segundoAno)-("+equipamentoPowerstrip+"*0.3*#terceiroAno)-("+equipamentoPowerstrip+"*0.4*#quartoAno)-("+equipamentoPowerstrip+"*0.5*#quintoAno)\"\n" +
                ");\n" +
                "\n" +
                "timedTransition MTTF_SDT(\n" +
                "inputs = [SDT_ON],\n" +
                "outputs = [SDT_OFF],\n" +
                "delay = \""+equipamentoTransformer+"-("+equipamentoTransformer+"*0.1*#primeiroAno)-("+equipamentoTransformer+"*0.2*#segundoAno)-("+equipamentoTransformer+"*0.3*#terceiroAno)-("+equipamentoTransformer+"*0.4*#quartoAno)-("+equipamentoTransformer+"*0.5*#quintoAno)\"\n" +
                ");\n" +
                "\n" +
                "timedTransition MTTF_SDT2(\n" +
                "inputs = [SDT_ON2],\n" +
                "outputs = [SDT_OFF2],\n" +
                "delay = \""+equipamentoTransformer+"-("+equipamentoTransformer+"*0.1*#primeiroAno)-("+equipamentoTransformer+"*0.2*#segundoAno)-("+equipamentoTransformer+"*0.3*#terceiroAno)-("+equipamentoTransformer+"*0.4*#quartoAno)-("+equipamentoTransformer+"*0.5*#quintoAno)\"\n" +
                ");\n" +
                "\n" +
                "timedTransition MTTF_STS(\n" +
                "inputs = [STS_ON],\n" +
                "outputs = [STS_OFF],\n" +
                "delay = \""+equipamentoSts+"-("+equipamentoSts+"*0.1*#primeiroAno)-("+equipamentoSts+"*0.2*#segundoAno)-("+equipamentoSts+"*0.3*#terceiroAno)-("+equipamentoSts+"*0.4*#quartoAno)-("+equipamentoSts+"*0.5*#quintoAno)\"\n" +
                ");\n" +
                "\n" +
                "timedTransition MTTF_SUB(\n" +
                "inputs = [SUB_ON],\n" +
                "outputs = [SUB_OFF],\n" +
                "delay = \""+equipamentoSubpainel+"-("+equipamentoSubpainel+"*0.1*#primeiroAno)-("+equipamentoSubpainel+"*0.2*#segundoAno)-("+equipamentoSubpainel+"*0.3*#terceiroAno)-("+equipamentoSubpainel+"*0.4*#quartoAno)-("+equipamentoSubpainel+"*0.5*#quintoAno)\"\n" +
                ");\n" +
                "\n" +
                "timedTransition MTTF_UPS(\n" +
                "inputs = [UPS_ON],\n" +
                "outputs = [UPS_OFF],\n" +
                "delay = \""+equipamentoUps+"-("+equipamentoUps+"*0.1*#primeiroAno)-("+equipamentoUps+"*0.2*#segundoAno)-("+equipamentoUps+"*0.3*#terceiroAno)-("+equipamentoUps+"*0.4*#quartoAno)-("+equipamentoUps+"*0.5*#quintoAno)\"\n" +
                ");\n" +
                "\n" +
                "timedTransition MTTF_UPS2(\n" +
                "inputs = [UPS_ON2],\n" +
                "outputs = [UPS_OFF2],\n" +
                "delay = \""+equipamentoUps+"-("+equipamentoUps+"*0.1*#primeiroAno)-("+equipamentoUps+"*0.2*#segundoAno)-("+equipamentoUps+"*0.3*#terceiroAno)-("+equipamentoUps+"*0.4*#quartoAno)-("+equipamentoUps+"*0.5*#quintoAno)\"\n" +
                ");\n" +
                "\n" +
                "timedTransition MTTR_POWE(\n" +
                "inputs = [REPARAR_POWE],\n" +
                "outputs = [POWE_ON, equipeManutencao],\n" +
                "delay = 8.0\n" +
                ");\n" +
                "\n" +
                "timedTransition MTTR_SDT(\n" +
                "inputs = [REPARAR_SDT],\n" +
                "outputs = [SDT_ON, equipeManutencao],\n" +
                "delay = 8.0\n" +
                ");\n" +
                "\n" +
                "timedTransition MTTR_SDT2(\n" +
                "inputs = [REPARAR_SDT2],\n" +
                "outputs = [SDT_ON2, equipeManutencao],\n" +
                "delay = 8.0\n" +
                ");\n" +
                "\n" +
                "timedTransition MTTR_STS(\n" +
                "inputs = [REPARAR_STS],\n" +
                "outputs = [STS_ON, equipeManutencao],\n" +
                "delay = 8.0\n" +
                ");\n" +
                "\n" +
                "timedTransition MTTR_SUB(\n" +
                "inputs = [REPARAR_SUB],\n" +
                "outputs = [SUB_ON, equipeManutencao],\n" +
                "delay = 8.0\n" +
                ");\n" +
                "\n" +
                "timedTransition MTTR_UPS(\n" +
                "inputs = [REPARAR_UPS],\n" +
                "outputs = [UPS_ON, equipeManutencao],\n" +
                "delay = 8.0\n" +
                ");\n" +
                "\n" +
                "timedTransition MTTR_UPS2(\n" +
                "inputs = [REPARAR_UPS2],\n" +
                "outputs = [UPS_ON2, equipeManutencao],\n" +
                "delay = 8.0\n" +
                ");\n" +
                "\n" +
                "timedTransition clockPrimeiroAno(\n" +
                "inputs = [inicial],\n" +
                "outputs = [primeiroAno],\n" +
                "delay = 8760.0\n" +
                ");\n" +
                "\n" +
                "timedTransition clockQuartoAno(\n" +
                "inputs = [terceiroAno],\n" +
                "outputs = [quartoAno],\n" +
                "delay = 8760.0\n" +
                ");\n" +
                "\n" +
                "timedTransition clockQuintoAno(\n" +
                "inputs = [quartoAno],\n" +
                "outputs = [quintoAno],\n" +
                "delay = 8760.0\n" +
                ");\n" +
                "\n" +
                "timedTransition clockSegundoAno(\n" +
                "inputs = [primeiroAno],\n" +
                "outputs = [segundoAno],\n" +
                "delay = 8760.0\n" +
                ");\n" +
                "\n" +
                "timedTransition clockTerceiroAno(\n" +
                "inputs = [segundoAno],\n" +
                "outputs = [terceiroAno],\n" +
                "delay = 8760.0\n" +
                ");\n" +
                "\n" +
                "metric av = stationaryAnalysis( expression = \"P{(((#UPS_ON=1)AND(#SDT_ON=1))OR((#UPS_ON2=1)AND(#SDT_ON2=1)))AND(#STS_ON=1)AND(#SUB_ON=1)AND(#POWE_ON=1)}\" );\n" +
                "}\n" +
                "\n" +
                "main {\n" +
                "av = solve( Model,av );\n" +
                "println(av);\n" +
                "\n" +
                "}";                
    }
    
    static double executarScriptA3(String equipamentoUps, String equipamentoTransformer, String equipamentoSubpainel, String equipamentoPowerstrip, String equipamentoSts, String ups, String sdt, String subpainel, String powerstrip, String sts, String qtdEquipe) throws IOException {
       
        ExecutionRuntime runtime = new ExecutionRuntime();
        //System.out.println(script(equipamentoUps, ups));
        Script script;
        script = new Script(scriptA3(equipamentoUps, equipamentoTransformer, equipamentoSubpainel, equipamentoPowerstrip, equipamentoSts, ups, sdt, subpainel, powerstrip, sts,qtdEquipe));
        //System.out.println(script);
        //System.exit(0);
        runtime.evaluateScript(script);

        double av;
        Metric Aval;

        Aval = runtime.getModel("Model").getMetric("av");
        av = Aval.solve();
        
        //escrever no arquivo
        //BufferedWriter bw = new BufferedWriter(new FileWriter("teste.txt", true));
        
        // Escreve e fecha arquivo
        //bw.write(av + "\t");
        //bw.close();
        return av;
    }
        
static String scriptA4(String equipamentoUps, String equipamentoTransformer, String equipamentoSubpainel, String equipamentoPowerstrip, String equipamentoSts, String ups, String sdt, String subpainel, String powerstrip, String sts, String qtdEquipe) {

    return "SPN Model{\n" +
            "\n" +
            "place POWE_OFF;\n" +
            "place POWE_ON( tokens= 1 );\n" +
            "place REPARAR_POWE;\n" +
            "place REPARAR_SDT;\n" +
            "place REPARAR_SDT2;\n" +
            "place REPARAR_STS;\n" +
            "place REPARAR_SUB;\n" +
            "place REPARAR_SUB2;\n" +
            "place REPARAR_UPS;\n" +
            "place REPARAR_UPS2;\n" +
            "place SDT_OFF;\n" +
            "place SDT_OFF2;\n" +
            "place SDT_ON( tokens= 1 );\n" +
            "place SDT_ON2( tokens= 1 );\n" +
            "place STS_OFF;\n" +
            "place STS_ON( tokens= 1 );\n" +
            "place SUB_OFF;\n" +
            "place SUB_OFF2;\n" +
            "place SUB_ON( tokens= 1 );\n" +
            "place SUB_ON2( tokens= 1 );\n" +
            "place UPS_OFF;\n" +
            "place UPS_OFF2;\n" +
            "place UPS_ON( tokens= 1 );\n" +
            "place UPS_ON2( tokens= 1 );\n" +
            "place equipeManutencao( tokens= "+ qtdEquipe +" );\n" +
            "place inicial( tokens= 1 );\n" +
            "place primeiroAno;\n" +
            "place quartoAno;\n" +
            "place quintoAno;\n" +
            "place segundoAno;\n" +
            "place terceiroAno;\n" +
            "\n" +
            "\n" +
            "immediateTransition acionarManutencaoPOWE(\n" +
            "inputs = [POWE_OFF, equipeManutencao],\n" +
            "outputs = [REPARAR_POWE]\n" +
            ");\n" +
            "\n" +
            "immediateTransition acionarManutencaoSDT(\n" +
            "inputs = [SDT_OFF, equipeManutencao],\n" +
            "outputs = [REPARAR_SDT]\n" +
            ");\n" +
            "\n" +
            "immediateTransition acionarManutencaoSDT2(\n" +
            "inputs = [SDT_OFF2, equipeManutencao],\n" +
            "outputs = [REPARAR_SDT2]\n" +
            ");\n" +
            "\n" +
            "immediateTransition acionarManutencaoSTS(\n" +
            "inputs = [STS_OFF, equipeManutencao],\n" +
            "outputs = [REPARAR_STS]\n" +
            ");\n" +
            "\n" +
            "immediateTransition acionarManutencaoSUB(\n" +
            "inputs = [SUB_OFF, equipeManutencao],\n" +
            "outputs = [REPARAR_SUB]\n" +
            ");\n" +
            "\n" +
            "immediateTransition acionarManutencaoSUB2(\n" +
            "inputs = [SUB_OFF2, equipeManutencao],\n" +
            "outputs = [REPARAR_SUB2]\n" +
            ");\n" +
            "\n" +
            "immediateTransition acionarManutencaoUPS(\n" +
            "inputs = [UPS_OFF, equipeManutencao],\n" +
            "outputs = [REPARAR_UPS]\n" +
            ");\n" +
            "\n" +
            "immediateTransition acionarManutencaoUPS2(\n" +
            "inputs = [UPS_OFF2, equipeManutencao],\n" +
            "outputs = [REPARAR_UPS2]\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTF_POWE(\n" +
            "inputs = [POWE_ON],\n" +
            "outputs = [POWE_OFF],\n" +
            "delay = \""+equipamentoPowerstrip+"-("+equipamentoPowerstrip+"*0.1*#primeiroAno)-("+equipamentoPowerstrip+"*0.2*#segundoAno)-("+equipamentoPowerstrip+"*0.3*#terceiroAno)-("+equipamentoPowerstrip+"*0.4*#quartoAno)-("+equipamentoPowerstrip+"*0.5*#quintoAno)\"\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTF_SDT(\n" +
            "inputs = [SDT_ON],\n" +
            "outputs = [SDT_OFF],\n" +
            "delay = \""+equipamentoTransformer+"-("+equipamentoTransformer+"*0.1*#primeiroAno)-("+equipamentoTransformer+"*0.2*#segundoAno)-("+equipamentoTransformer+"*0.3*#terceiroAno)-("+equipamentoTransformer+"*0.4*#quartoAno)-("+equipamentoTransformer+"*0.5*#quintoAno)\"\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTF_SDT2(\n" +
            "inputs = [SDT_ON2],\n" +
            "outputs = [SDT_OFF2],\n" +
            "delay = \""+equipamentoTransformer+"-("+equipamentoTransformer+"*0.1*#primeiroAno)-("+equipamentoTransformer+"*0.2*#segundoAno)-("+equipamentoTransformer+"*0.3*#terceiroAno)-("+equipamentoTransformer+"*0.4*#quartoAno)-("+equipamentoTransformer+"*0.5*#quintoAno)\"\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTF_STS(\n" +
            "inputs = [STS_ON],\n" +
            "outputs = [STS_OFF],\n" +
            "delay = \""+equipamentoSts+"-("+equipamentoSts+"*0.1*#primeiroAno)-("+equipamentoSts+"*0.2*#segundoAno)-("+equipamentoSts+"*0.3*#terceiroAno)-("+equipamentoSts+"*0.4*#quartoAno)-("+equipamentoSts+"*0.5*#quintoAno)\"\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTF_SUB(\n" +
            "inputs = [SUB_ON],\n" +
            "outputs = [SUB_OFF],\n" +
            "delay = \""+equipamentoSubpainel+"-("+equipamentoSubpainel+"*0.1*#primeiroAno)-("+equipamentoSubpainel+"*0.2*#segundoAno)-("+equipamentoSubpainel+"*0.3*#terceiroAno)-("+equipamentoSubpainel+"*0.4*#quartoAno)-("+equipamentoSubpainel+"*0.5*#quintoAno)\"\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTF_SUB2(\n" +
            "inputs = [SUB_ON2],\n" +
            "outputs = [SUB_OFF2],\n" +
            "delay = \""+equipamentoSubpainel+"-("+equipamentoSubpainel+"*0.1*#primeiroAno)-("+equipamentoSubpainel+"*0.2*#segundoAno)-("+equipamentoSubpainel+"*0.3*#terceiroAno)-("+equipamentoSubpainel+"*0.4*#quartoAno)-("+equipamentoSubpainel+"*0.5*#quintoAno)\"\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTF_UPS(\n" +
            "inputs = [UPS_ON],\n" +
            "outputs = [UPS_OFF],\n" +
            "delay = \""+equipamentoUps+"-("+equipamentoUps+"*0.1*#primeiroAno)-("+equipamentoUps+"*0.2*#segundoAno)-("+equipamentoUps+"*0.3*#terceiroAno)-("+equipamentoUps+"*0.4*#quartoAno)-("+equipamentoUps+"*0.5*#quintoAno)\"\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTF_UPS2(\n" +
            "inputs = [UPS_ON2],\n" +
            "outputs = [UPS_OFF2],\n" +
            "delay = \""+equipamentoUps+"-("+equipamentoUps+"*0.1*#primeiroAno)-("+equipamentoUps+"*0.2*#segundoAno)-("+equipamentoUps+"*0.3*#terceiroAno)-("+equipamentoUps+"*0.4*#quartoAno)-("+equipamentoUps+"*0.5*#quintoAno)\"\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTR_POWE(\n" +
            "inputs = [REPARAR_POWE],\n" +
            "outputs = [POWE_ON, equipeManutencao],\n" +
            "delay = 8.0\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTR_SDT(\n" +
            "inputs = [REPARAR_SDT],\n" +
            "outputs = [SDT_ON, equipeManutencao],\n" +
            "delay = 8.0\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTR_SDT2(\n" +
            "inputs = [REPARAR_SDT2],\n" +
            "outputs = [SDT_ON2, equipeManutencao],\n" +
            "delay = 8.0\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTR_STS(\n" +
            "inputs = [REPARAR_STS],\n" +
            "outputs = [STS_ON, equipeManutencao],\n" +
            "delay = 8.0\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTR_SUB(\n" +
            "inputs = [REPARAR_SUB],\n" +
            "outputs = [SUB_ON, equipeManutencao],\n" +
            "delay = 8.0\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTR_SUB2(\n" +
            "inputs = [REPARAR_SUB2],\n" +
            "outputs = [SUB_ON2, equipeManutencao],\n" +
            "delay = 8.0\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTR_UPS(\n" +
            "inputs = [REPARAR_UPS],\n" +
            "outputs = [UPS_ON, equipeManutencao],\n" +
            "delay = 8.0\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTR_UPS2(\n" +
            "inputs = [REPARAR_UPS2],\n" +
            "outputs = [UPS_ON2, equipeManutencao],\n" +
            "delay = 8.0\n" +
            ");\n" +
            "\n" +
            "timedTransition clockPrimeiroAno(\n" +
            "inputs = [inicial],\n" +
            "outputs = [primeiroAno],\n" +
            "delay = 8760.0\n" +
            ");\n" +
            "\n" +
            "timedTransition clockQuartoAno(\n" +
            "inputs = [terceiroAno],\n" +
            "outputs = [quartoAno],\n" +
            "delay = 8760.0\n" +
            ");\n" +
            "\n" +
            "timedTransition clockQuintoAno(\n" +
            "inputs = [quartoAno],\n" +
            "outputs = [quintoAno],\n" +
            "delay = 8760.0\n" +
            ");\n" +
            "\n" +
            "timedTransition clockSegundoAno(\n" +
            "inputs = [primeiroAno],\n" +
            "outputs = [segundoAno],\n" +
            "delay = 8760.0\n" +
            ");\n" +
            "\n" +
            "timedTransition clockTerceiroAno(\n" +
            "inputs = [segundoAno],\n" +
            "outputs = [terceiroAno],\n" +
            "delay = 8760.0\n" +
            ");\n" +
            "\n" +
            "metric av = stationaryAnalysis( expression = \"P{(((#UPS_ON=1)AND(#SDT_ON=1)AND(#SUB_ON=1))OR((#UPS_ON2=1)AND(#SDT_ON2=1)AND(#SUB_ON2=1)))AND(#STS_ON=1)AND(#POWE_ON=1)}\" );\n" +
            "}\n" +
            "\n" +
            "main {\n" +
            "av = solve( Model,av );\n" +
            "println(av);\n" +
            "\n" +
            "}";
    }
    
    static double executarScriptA4(String equipamentoUps, String equipamentoTransformer, String equipamentoSubpainel, String equipamentoPowerstrip, String equipamentoSts, String ups, String sdt, String subpainel, String powerstrip, String sts, String qtdEquipe) throws IOException {
       
        ExecutionRuntime runtime = new ExecutionRuntime();
        //System.out.println(script(equipamentoUps, ups));
        Script script;
        script = new Script(scriptA4(equipamentoUps, equipamentoTransformer, equipamentoSubpainel, equipamentoPowerstrip, equipamentoSts, ups, sdt, subpainel, powerstrip, sts, qtdEquipe));
        //System.out.println(script);
        //System.exit(0);s
        runtime.evaluateScript(script);

        double av;
        Metric Aval;

        Aval = runtime.getModel("Model").getMetric("av");
        av = Aval.solve();
        
        //escrever no arquivo
        //BufferedWriter bw = new BufferedWriter(new FileWriter("teste.txt", true));
        
        // Escreve e fecha arquivo
        //bw.write(av + "\t");
        //bw.close();
        return av;
    }      

static String scriptA5(String equipamentoUps, String equipamentoTransformer, String equipamentoSubpainel, String equipamentoPowerstrip, String equipamentoSts, String ups, String sdt, String subpainel, String powerstrip, String sts, String qtdEquipe) {

    return "SPN Model{\n" +
"\n" +
"place POWE_OFF;\n" +
"place POWE_OFF2;\n" +
"place POWE_ON( tokens= 1 );\n" +
"place POWE_ON2( tokens= 1 );\n" +
"place REPARAR_POWE;\n" +
"place REPARAR_POWE2;\n" +
"place REPARAR_SDT;\n" +
"place REPARAR_SDT2;\n" +
"place REPARAR_SUB;\n" +
"place REPARAR_SUB2;\n" +
"place REPARAR_UPS;\n" +
"place REPARAR_UPS2;\n" +
"place SDT_OFF;\n" +
"place SDT_OFF2;\n" +
"place SDT_ON( tokens= 1 );\n" +
"place SDT_ON2( tokens= 1 );\n" +
"place SUB_OFF;\n" +
"place SUB_OFF2;\n" +
"place SUB_ON( tokens= 1 );\n" +
"place SUB_ON2( tokens= 1 );\n" +
"place UPS_OFF;\n" +
"place UPS_OFF2;\n" +
"place UPS_ON( tokens= 1 );\n" +
"place UPS_ON2( tokens= 1 );\n" +
"place equipeManutencao( tokens= "+ qtdEquipe +" );\n" +
"place inicial( tokens= 1 );\n" +
"place primeiroAno;\n" +
"place quartoAno;\n" +
"place quintoAno;\n" +
"place segundoAno;\n" +
"place terceiroAno;\n" +
"\n" +
"\n" +
"immediateTransition acionarManutencaoPOWE(\n" +
"inputs = [POWE_OFF, equipeManutencao],\n" +
"outputs = [REPARAR_POWE]\n" +
");\n" +
"\n" +
"immediateTransition acionarManutencaoPOWE2(\n" +
"inputs = [POWE_OFF2, equipeManutencao],\n" +
"outputs = [REPARAR_POWE2]\n" +
");\n" +
"\n" +
"immediateTransition acionarManutencaoSDT(\n" +
"inputs = [SDT_OFF, equipeManutencao],\n" +
"outputs = [REPARAR_SDT]\n" +
");\n" +
"\n" +
"immediateTransition acionarManutencaoSDT2(\n" +
"inputs = [SDT_OFF2, equipeManutencao],\n" +
"outputs = [REPARAR_SDT2]\n" +
");\n" +
"\n" +
"immediateTransition acionarManutencaoSUB(\n" +
"inputs = [SUB_OFF, equipeManutencao],\n" +
"outputs = [REPARAR_SUB]\n" +
");\n" +
"\n" +
"immediateTransition acionarManutencaoSUB(\n" +
"inputs = [SUB_OFF2, equipeManutencao],\n" +
"outputs = [REPARAR_SUB2]\n" +
");\n" +
"\n" +
"immediateTransition acionarManutencaoUPS(\n" +
"inputs = [UPS_OFF, equipeManutencao],\n" +
"outputs = [REPARAR_UPS]\n" +
");\n" +
"\n" +
"immediateTransition acionarManutencaoUPS2(\n" +
"inputs = [UPS_OFF2, equipeManutencao],\n" +
"outputs = [REPARAR_UPS2]\n" +
");\n" +
"\n" +
"timedTransition MTTF_POWE(\n" +
"inputs = [POWE_ON],\n" +
"outputs = [POWE_OFF],\n" +
"delay = \""+equipamentoPowerstrip+"-("+equipamentoPowerstrip+"*0.1*#primeiroAno)-("+equipamentoPowerstrip+"*0.2*#segundoAno)-("+equipamentoPowerstrip+"*0.3*#terceiroAno)-("+equipamentoPowerstrip+"*0.4*#quartoAno)-("+equipamentoPowerstrip+"*0.5*#quintoAno)\"\n" +
");\n" +
"\n" +
"timedTransition MTTF_POWE2(\n" +
"inputs = [POWE_ON2],\n" +
"outputs = [POWE_OFF2],\n" +
"delay = \""+equipamentoPowerstrip+"-("+equipamentoPowerstrip+"*0.1*#primeiroAno)-("+equipamentoPowerstrip+"*0.2*#segundoAno)-("+equipamentoPowerstrip+"*0.3*#terceiroAno)-("+equipamentoPowerstrip+"*0.4*#quartoAno)-("+equipamentoPowerstrip+"*0.5*#quintoAno)\"\n" +
");\n" +
"\n" +
"timedTransition MTTF_SDT(\n" +
"inputs = [SDT_ON],\n" +
"outputs = [SDT_OFF],\n" +
"delay = \""+equipamentoTransformer+"-("+equipamentoTransformer+"*0.1*#primeiroAno)-("+equipamentoTransformer+"*0.2*#segundoAno)-("+equipamentoTransformer+"*0.3*#terceiroAno)-("+equipamentoTransformer+"*0.4*#quartoAno)-("+equipamentoTransformer+"*0.5*#quintoAno)\"\n" +
");\n" +
"\n" +
"timedTransition MTTF_SDT2(\n" +
"inputs = [SDT_ON2],\n" +
"outputs = [SDT_OFF2],\n" +
"delay = \""+equipamentoTransformer+"-("+equipamentoTransformer+"*0.1*#primeiroAno)-("+equipamentoTransformer+"*0.2*#segundoAno)-("+equipamentoTransformer+"*0.3*#terceiroAno)-("+equipamentoTransformer+"*0.4*#quartoAno)-("+equipamentoTransformer+"*0.5*#quintoAno)\"\n" +
");\n" +
"\n" +
"timedTransition MTTF_SUB(\n" +
"inputs = [SUB_ON],\n" +
"outputs = [SUB_OFF],\n" +
"delay = \""+equipamentoSubpainel+"-("+equipamentoSubpainel+"*0.1*#primeiroAno)-("+equipamentoSubpainel+"*0.2*#segundoAno)-("+equipamentoSubpainel+"*0.3*#terceiroAno)-("+equipamentoSubpainel+"*0.4*#quartoAno)-("+equipamentoSubpainel+"*0.5*#quintoAno)\"\n" +
");\n" +
"\n" +
"timedTransition MTTF_SUB2(\n" +
"inputs = [SUB_ON2],\n" +
"outputs = [SUB_OFF2],\n" +
"delay = \""+equipamentoSubpainel+"-("+equipamentoSubpainel+"*0.1*#primeiroAno)-("+equipamentoSubpainel+"*0.2*#segundoAno)-("+equipamentoSubpainel+"*0.3*#terceiroAno)-("+equipamentoSubpainel+"*0.4*#quartoAno)-("+equipamentoSubpainel+"*0.5*#quintoAno)\"\n" +
");\n" +
"\n" +
"timedTransition MTTF_UPS(\n" +
"inputs = [UPS_ON],\n" +
"outputs = [UPS_OFF],\n" +
"delay = \""+equipamentoUps+"-("+equipamentoUps+"*0.1*#primeiroAno)-("+equipamentoUps+"*0.2*#segundoAno)-("+equipamentoUps+"*0.3*#terceiroAno)-("+equipamentoUps+"*0.4*#quartoAno)-("+equipamentoUps+"*0.5*#quintoAno)\"\n" +
");\n" +
"\n" +
"timedTransition MTTF_UPS2(\n" +
"inputs = [UPS_ON2],\n" +
"outputs = [UPS_OFF2],\n" +
"delay = \""+equipamentoUps+"-("+equipamentoUps+"*0.1*#primeiroAno)-("+equipamentoUps+"*0.2*#segundoAno)-("+equipamentoUps+"*0.3*#terceiroAno)-("+equipamentoUps+"*0.4*#quartoAno)-("+equipamentoUps+"*0.5*#quintoAno)\"\n" +
");\n" +
"\n" +
"timedTransition MTTR_POWE(\n" +
"inputs = [REPARAR_POWE],\n" +
"outputs = [POWE_ON, equipeManutencao],\n" +
"delay = 8.0\n" +
");\n" +
"\n" +
"timedTransition MTTR_POWE2(\n" +
"inputs = [REPARAR_POWE2],\n" +
"outputs = [POWE_ON2, equipeManutencao],\n" +
"delay = 8.0\n" +
");\n" +
"\n" +
"timedTransition MTTR_SDT(\n" +
"inputs = [REPARAR_SDT],\n" +
"outputs = [SDT_ON, equipeManutencao],\n" +
"delay = 8.0\n" +
");\n" +
"\n" +
"timedTransition MTTR_SDT2(\n" +
"inputs = [REPARAR_SDT2],\n" +
"outputs = [SDT_ON2, equipeManutencao],\n" +
"delay = 8.0\n" +
");\n" +
"\n" +
"timedTransition MTTR_SUB(\n" +
"inputs = [REPARAR_SUB],\n" +
"outputs = [SUB_ON, equipeManutencao],\n" +
"delay = 8.0\n" +
");\n" +
"\n" +
"timedTransition MTTR_SUB2(\n" +
"inputs = [REPARAR_SUB2],\n" +
"outputs = [SUB_ON2, equipeManutencao],\n" +
"delay = 8.0\n" +
");\n" +
"\n" +
"timedTransition MTTR_UPS(\n" +
"inputs = [REPARAR_UPS],\n" +
"outputs = [UPS_ON, equipeManutencao],\n" +
"delay = 8.0\n" +
");\n" +
"\n" +
"timedTransition MTTR_UPS2(\n" +
"inputs = [REPARAR_UPS2],\n" +
"outputs = [UPS_ON2, equipeManutencao],\n" +
"delay = 8.0\n" +
");\n" +
"\n" +
"timedTransition clockPrimeiroAno(\n" +
"inputs = [inicial],\n" +
"outputs = [primeiroAno],\n" +
"delay = 8760.0\n" +
");\n" +
"\n" +
"timedTransition clockQuartoAno(\n" +
"inputs = [terceiroAno],\n" +
"outputs = [quartoAno],\n" +
"delay = 8760.0\n" +
");\n" +
"\n" +
"timedTransition clockQuintoAno(\n" +
"inputs = [quartoAno],\n" +
"outputs = [quintoAno],\n" +
"delay = 8760.0\n" +
");\n" +
"\n" +
"timedTransition clockSegundoAno(\n" +
"inputs = [primeiroAno],\n" +
"outputs = [segundoAno],\n" +
"delay = 8760.0\n" +
");\n" +
"\n" +
"timedTransition clockTerceiroAno(\n" +
"inputs = [segundoAno],\n" +
"outputs = [terceiroAno],\n" +
"delay = 8760.0\n" +
");\n" +
"\n" +
"metric av = stationaryAnalysis( expression = \"P{(((#UPS_ON=1)AND(#SDT_ON=1)AND(#SUB_ON=1)AND(#POWE_ON=1))OR((#UPS_ON2=1)AND(#SDT_ON2=1)AND(#SUB_ON2=1)AND(#POWE_ON2=1)))}\" );\n" +
"}\n" +
"\n" +
"main {\n" +
"av = solve( Model,av );\n" +
"println(av);\n" +
"\n" +
"}";
    }
    
        static double executarScriptA5(String equipamentoUps, String equipamentoTransformer, String equipamentoSubpainel, String equipamentoPowerstrip, String equipamentoSts, String ups, String sdt, String subpainel, String powerstrip, String sts, String qtdEquipe) throws IOException {
       
        ExecutionRuntime runtime = new ExecutionRuntime();
        //System.out.println(script(equipamentoUps, ups));
        Script script;
        script = new Script(scriptA5(equipamentoUps, equipamentoTransformer, equipamentoSubpainel, equipamentoPowerstrip, equipamentoSts, ups, sdt, subpainel, powerstrip, sts, qtdEquipe));
        //System.out.println(script);
        //System.exit(0);s
        runtime.evaluateScript(script);

        double av;
        Metric Aval;

        Aval = runtime.getModel("Model").getMetric("av");
        av = Aval.solve();
        
        //escrever no arquivo
        //BufferedWriter bw = new BufferedWriter(new FileWriter("teste.txt", true));
        
        // Escreve e fecha arquivo
        //bw.write(av + "\t");
        //bw.close();
        return av;
    }  

    double executarScriptA2(double executarScriptA2) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


}
