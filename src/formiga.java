import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import org.modcs.tools.parser.model.ExecutionRuntime;
import org.modcs.tools.parser.model.Script;
import org.modcs.tools.parser.model.metrics.Metric;

public class formiga {

    public static void main(String[] args) {

        try {
            calcula calculo = new calcula(); 
            
            ArrayList ups = new ArrayList();
            ArrayList subpainel = new ArrayList();
            ArrayList powerstrip = new ArrayList();
            ArrayList transformer = new ArrayList();
            BufferedReader buffer = new BufferedReader(new FileReader("C://Users/fmelo/Desktop/Components270000g2.txt"));
            
            String linha = null;
            while ((linha = buffer.readLine()) != null) {
                String[] linhaSeparado = linha.split(";");
                String equipamento = linhaSeparado[1];
                if (equipamento.equals("UPS_5KVA0")) {
                    ups.add(linhaSeparado);
                }
                if (equipamento.equals("SDTransformer1")) {
                    transformer.add(linhaSeparado);
                }
                if (equipamento.equals("Subpanel2")) {
                    subpainel.add(linhaSeparado);
                }
                if (equipamento.equals("PowerStrip3")) {
                    powerstrip.add(linhaSeparado);
                }
            }
            for (int u = 0; u < ups.size(); ++u) {
                String[] equipamentoUps = (String[]) ups.get(u);
                double resultado = calculo.executarScriptSimplificado(equipamentoUps[3], equipamentoUps[1]);      
                
                BufferedWriter bw = new BufferedWriter(new FileWriter("teste.txt", true));
                System.out.print(resultado + "\t");
                bw.write(resultado + "\t");
                bw.close();
                
            }
            
            for (int t = 0; t < transformer.size(); ++t) {
                String[] equipamentoTransformer = (String[]) transformer.get(t);
                double resultado = calculo.executarScriptSimplificado(equipamentoTransformer[3], equipamentoTransformer[1]);
                 
                BufferedWriter bw = new BufferedWriter(new FileWriter("teste.txt", true));                
                bw.write(resultado + "\t");
                bw.close();
            }

            for (int s = 0; s < subpainel.size(); ++s) {
                String[] equipamentoSubpainel = (String[]) subpainel.get(s);
                double resultado = calculo.executarScriptSimplificado(equipamentoSubpainel[3], equipamentoSubpainel[1]);
                
                BufferedWriter bw = new BufferedWriter(new FileWriter("teste.txt", true));
                bw.write(resultado + "\t");
                bw.close();
            }
            for (int p = 0; p < powerstrip.size(); ++p) {
                String[] equipamentoPowerstrip = (String[]) powerstrip.get(p);
                double resultado = calculo.executarScriptSimplificado(equipamentoPowerstrip[3], equipamentoPowerstrip[1]);
                
                BufferedWriter bw = new BufferedWriter(new FileWriter("teste.txt", true));
                bw.write(resultado + "\t");
                bw.close();

            }
        } catch (IOException e) {
            System.out.print("Erro na abertura do arquivo: %s.\n");
        }

        System.out.println();
    }
    private String arq;
}
