import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import org.modcs.tools.parser.model.ExecutionRuntime;
import org.modcs.tools.parser.model.Script;
import org.modcs.tools.parser.model.metrics.Metric;

public class disponibilidadeEquipamentos {

    public static void main(String[] args) {

        try {            
            calcula calculo = new calcula(); 
            
            ArrayList ups = new ArrayList();
            ArrayList subpainel = new ArrayList();
            ArrayList powerstrip = new ArrayList();
            ArrayList transformer = new ArrayList();
            ArrayList sts = new ArrayList();
            BufferedReader buffer = new BufferedReader(new FileReader("C://Users/felip/Desktop/Components270000g1.txt"));
            String linha = null;
            while ((linha = buffer.readLine()) != null) {
                String[] linhaSeparado = linha.split(";");
                String equipamento = linhaSeparado[1];
                if (equipamento.equals("UPS_5KVA0")) {
                    ups.add(linhaSeparado);
                }
                if (equipamento.equals("SDTransformer2")) {
                    transformer.add(linhaSeparado);
                }
                if (equipamento.equals("Subpanel5")) {
                    subpainel.add(linhaSeparado);
                }
                if (equipamento.equals("PowerStrip6")) {
                    powerstrip.add(linhaSeparado);
                }
                
               if (equipamento.equals("STS4")) {
                   sts.add(linhaSeparado);
               }
            }
            for (int u = 0; u < ups.size(); ++u) {
                String[] equipamentoUps = (String[]) ups.get(u);
                double upsValor = calculo.executarScriptSimplificado(equipamentoUps[3], equipamentoUps[1]);
                
                String[] equipamentoTransformer = (String[]) transformer.get(u);
                double transformerValor = calculo.executarScriptSimplificado(equipamentoTransformer[3], equipamentoTransformer[1]);
            
                String[] equipamentoSubpainel = (String[]) subpainel.get(u);
                double subpainelValor = calculo.executarScriptSimplificado(equipamentoSubpainel[3], equipamentoSubpainel[1]);
           
                String[] equipamentoPowerstrip = (String[]) powerstrip.get(u);
                double powerstripValor = calculo.executarScriptSimplificado(equipamentoPowerstrip[3], equipamentoPowerstrip[1]);

                String[] equipamentoSts = (String[]) sts.get(u);
                double stsValor = calculo.executarScriptSimplificado(equipamentoSts[3], equipamentoSts[1]);

                
                //escrever no arquivo
                BufferedWriter bw = new BufferedWriter(new FileWriter("diponibilidades.txt", true));

                // Escreve e fecha arquivo
               // bw.write(upsValor + "\t" + transformerValor + "\n");
                bw.write(upsValor + "\t" + transformerValor + "\t" + subpainelValor  + "\t" + powerstripValor +  "\t" + stsValor + "\n");
                bw.close();
        
            }
        } catch (IOException e) {
            System.out.print("Erro na abertura do arquivo: %s.\n");
        }

        System.out.println();
    }
    private String arq;
}
