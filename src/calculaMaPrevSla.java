import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import org.modcs.tools.parser.model.ExecutionRuntime;
import org.modcs.tools.parser.model.Script;
import org.modcs.tools.parser.model.metrics.Metric;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author fmelo
 */
public class calculaMaPrevSla {
        
    static String scriptSimplificado(String equipamentoUps, String ups) {

        return "SPN Model{ \n"
                + "place " + ups + "_on( tokens= 1 ); \n"
                + "place " + ups + "_off; \n"
                + "timedTransition failure" + ups.substring(0, 1).toUpperCase() + ups.substring(1) + "(\n"
                + "inputs = [" + ups + "_on],\n"
                + "outputs = [" + ups + "_off],\n"
                + "delay = " + equipamentoUps + ");\n"
                + "timedTransition repair" + ups.substring(0, 1).toUpperCase() + ups.substring(1) + "(\n"
                + "inputs = [" + ups + "_off],\n"
                + "outputs = [" + ups + "_on],\n"
                + "delay = 8.0);\n"
                + "metric av = stationaryAnalysis( expression = \"P{(#" + ups + "_on=1)}\");}\n"
                + "main {\n"
                + "av = solve( Model,av );\n"
                + "println(av);}\n";
    }
    
      static double executarScriptSimplificado(String equipamentoUps, String ups) throws IOException {
       
        ExecutionRuntime runtime = new ExecutionRuntime();
        //System.out.println(script(equipamentoUps, ups));
        Script script = new Script(scriptSimplificado(equipamentoUps, ups));
        runtime.evaluateScript(script);

        double av;
        Metric Aval;

        Aval = runtime.getModel("Model").getMetric("av");
        av = Aval.solve();
        
        //escrever no arquivo
       // BufferedWriter bw = new BufferedWriter(new FileWriter("teste.txt", true));
        
        // Escreve e fecha arquivo
        //bw.write(av + "\t");
       // bw.close();
        return av;
    }
      
    static String scriptA2(String equipamentoUps, String equipamentoTransformer, String equipamentoSubpainel, String equipamentoPowerstrip, String equipamentoSts, String ups, String sdt, String subpainel, String powerstrip, String sts,String qtEquipe, int sla, int slaPreventiva) {

        //System.out.println(equipamentoUps + " -- "+ quipamentoTransformer+ " -- "+equipamentoSubpainel+ " -- "+equipamentoPowerstrip+ " -- "+equipamentoSts);
        
        return "SPN Model{\n" +
                "\n" +
                "place P0;\n" +
                "place P1;\n" +
                "place POWE_OFF;\n" +
                "place POWE_ON( tokens= 1 );\n" +
                "place REPARAR_POWE;\n" +
                "place REPARAR_SDT;\n" +
                "place REPARAR_STS;\n" +
                "place REPARAR_SUB;\n" +
                "place REPARAR_UPS;\n" +
                "place REPARAR_UPS2;\n" +
                "place SDT_OFF;\n" +
                "place SDT_ON( tokens= 1 );\n" +
                "place STS_OFF;\n" +
                "place STS_ON( tokens= 1 );\n" +
                "place SUB_OFF;\n" +
                "place SUB_ON( tokens= 1 );\n" +
                "place UPS_OFF;\n" +
                "place UPS_OFF2;\n" +
                "place UPS_ON( tokens= 1 );\n" +
                "place UPS_ON2( tokens= 1 );\n" +
                "place equipeManutencao( tokens= "+qtEquipe+" );\n" +
                "place inicial( tokens= 1 );\n" +
                "place primeiroAno;\n" +
                "place quartoAno;\n" +
                "place quintoAno;\n" +
                "place segundoAno;\n" +
                "place terceiroAno;\n" +
                "\n" +
                "\n" +
                "immediateTransition TI0(\n" +
                "enablingFunction = \"(#P0=1)OR(#P1=1)\",\n" +
                "inputs = [primeiroAno],\n" +
                "outputs = [inicial]\n" +
                ");\n" +
                "\n" +
                "immediateTransition TI1(\n" +
                "enablingFunction = \"(#P0=1)OR(#P1=1)\",\n" +
                "inputs = [segundoAno],\n" +
                "outputs = [inicial]\n" +
                ");\n" +
                "\n" +
                "immediateTransition TI2(\n" +
                "enablingFunction = \"(#P0=1)OR(#P1=1)\",\n" +
                "inputs = [terceiroAno],\n" +
                "outputs = [inicial]\n" +
                ");\n" +
                "\n" +
                "immediateTransition TI3(\n" +
                "enablingFunction = \"(#P0=1)OR(#P1=1)\",\n" +
                "inputs = [quartoAno],\n" +
                "outputs = [inicial]\n" +
                ");\n" +
                "\n" +
                "immediateTransition TI4(\n" +
                "enablingFunction = \"(#P0=1)OR(#P1=1)\",\n" +
                "inputs = [quintoAno],\n" +
                "outputs = [inicial]\n" +
                ");\n" +
                "\n" +
                "immediateTransition acionarManutencaoPOWE(\n" +
                "inputs = [POWE_OFF, equipeManutencao],\n" +
                "outputs = [REPARAR_POWE]\n" +
                ");\n" +
                "\n" +
                "immediateTransition acionarManutencaoSDT(\n" +
                "inputs = [SDT_OFF, equipeManutencao],\n" +
                "outputs = [REPARAR_SDT]\n" +
                ");\n" +
                "\n" +
                "immediateTransition acionarManutencaoSTS(\n" +
                "inputs = [STS_OFF, equipeManutencao],\n" +
                "outputs = [REPARAR_STS]\n" +
                ");\n" +
                "\n" +
                "immediateTransition acionarManutencaoSUB(\n" +
                "inputs = [SUB_OFF, equipeManutencao],\n" +
                "outputs = [REPARAR_SUB]\n" +
                ");\n" +
                "\n" +
                "immediateTransition acionarManutencaoUPS(\n" +
                "inputs = [UPS_OFF, equipeManutencao],\n" +
                "outputs = [REPARAR_UPS]\n" +
                ");\n" +
                "\n" +
                "immediateTransition acionarManutencaoUPS2(\n" +
                "inputs = [UPS_OFF2, equipeManutencao],\n" +
                "outputs = [REPARAR_UPS2]\n" +
                ");\n" +
                "\n" +
                "timedTransition MTTF_POWE(\n" +
                "inputs = [POWE_ON],\n" +
                "outputs = [POWE_OFF],\n" +
                "delay = \""+equipamentoPowerstrip+"-("+equipamentoPowerstrip+"*0.1*#primeiroAno)-("+equipamentoPowerstrip+"*0.2*#segundoAno)-("+equipamentoPowerstrip+"*0.3*#terceiroAno)-("+equipamentoPowerstrip+"*0.4*#quartoAno)-("+equipamentoPowerstrip+"*0.5*#quintoAno)\"\n" +
                ");\n" +
                "\n" +
                "timedTransition MTTF_SDT(\n" +
                "inputs = [SDT_ON],\n" +
                "outputs = [SDT_OFF],\n" +
                "delay = \""+equipamentoTransformer+"-("+equipamentoTransformer+"*0.1*#primeiroAno)-("+equipamentoTransformer+"*0.2*#segundoAno)-("+equipamentoTransformer+"*0.3*#terceiroAno)-("+equipamentoTransformer+"*0.4*#quartoAno)-("+equipamentoTransformer+"*0.5*#quintoAno)\"\n" +
                ");\n" +
                "\n" +
                "timedTransition MTTF_STS(\n" +
                "inputs = [STS_ON],\n" +
                "outputs = [STS_OFF],\n" +
                "delay = \""+equipamentoSts+"-("+equipamentoSts+"*0.1*#primeiroAno)-("+equipamentoSts+"*0.2*#segundoAno)-("+equipamentoSts+"*0.3*#terceiroAno)-("+equipamentoSts+"*0.4*#quartoAno)-("+equipamentoSts+"*0.5*#quintoAno)\"\n" +
                ");\n" +
                "\n" +
                "timedTransition MTTF_SUB(\n" +
                "inputs = [SUB_ON],\n" +
                "outputs = [SUB_OFF],\n" +
                "delay = \""+equipamentoSubpainel+"-("+equipamentoSubpainel+"*0.1*#primeiroAno)-("+equipamentoSubpainel+"*0.2*#segundoAno)-("+equipamentoSubpainel+"*0.3*#terceiroAno)-("+equipamentoSubpainel+"*0.4*#quartoAno)-("+equipamentoSubpainel+"*0.5*#quintoAno)\"\n" +
                ");\n" +
                "\n" +
                "timedTransition MTTF_UPS(\n" +
                "inputs = [UPS_ON],\n" +
                "outputs = [UPS_OFF],\n" +
                "delay = \""+equipamentoUps+"-("+equipamentoUps+"*0.1*#primeiroAno)-("+equipamentoUps+"*0.2*#segundoAno)-("+equipamentoUps+"*0.3*#terceiroAno)-("+equipamentoUps+"*0.4*#quartoAno)-("+equipamentoUps+"*0.5*#quintoAno)\"\n" +
                ");\n" +
                "\n" +
                "timedTransition MTTF_UPS2(\n" +
                "inputs = [UPS_ON2],\n" +
                "outputs = [UPS_OFF2],\n" +
                "delay = \""+equipamentoUps+"-("+equipamentoUps+"*0.1*#primeiroAno)-("+equipamentoUps+"*0.2*#segundoAno)-("+equipamentoUps+"*0.3*#terceiroAno)-("+equipamentoUps+"*0.4*#quartoAno)-("+equipamentoUps+"*0.5*#quintoAno)\"\n" +
                ");\n" +
                "\n" +
                "timedTransition MTTR_POWE(\n" +
                "inputs = [REPARAR_POWE],\n" +
                "outputs = [POWE_ON, equipeManutencao],\n" +
                "delay = 8.0\n" +
                ");\n" +
                "\n" +
                "timedTransition MTTR_SDT(\n" +
                "inputs = [REPARAR_SDT],\n" +
                "outputs = [SDT_ON, equipeManutencao],\n" +
                "delay = 8.0\n" +
                ");\n" +
                "\n" +
                "timedTransition MTTR_STS(\n" +
                "inputs = [REPARAR_STS],\n" +
                "outputs = [STS_ON, equipeManutencao],\n" +
                "delay = 8.0\n" +
                ");\n" +
                "\n" +
                "timedTransition MTTR_SUB(\n" +
                "inputs = [REPARAR_SUB],\n" +
                "outputs = [SUB_ON, equipeManutencao],\n" +
                "delay = 8.0\n" +
                ");\n" +
                "\n" +
                "timedTransition MTTR_UPS(\n" +
                "inputs = [REPARAR_UPS],\n" +
                "outputs = [UPS_ON, equipeManutencao],\n" +
                "delay = "+sla+"\n" +
                ");\n" +
                "\n" +
                "timedTransition MTTR_UPS2(\n" +
                "inputs = [REPARAR_UPS2],\n" +
                "outputs = [UPS_ON2, equipeManutencao],\n" +
                "delay = "+sla+"\n" +
                ");\n" +
                "\n" +
                "timedTransition TE0(\n" +
                "inputs = [UPS_ON, UPS_ON2, equipeManutencao],\n" +
                "outputs = [P0, UPS_ON2],\n" +
                "delay = "+slaPreventiva+"\n" +
                ");\n" +
                "\n" +
                "timedTransition TE1(\n" +
                "inputs = [P0],\n" +
                "outputs = [UPS_ON, equipeManutencao],\n" +
                "delay = 4.0\n" +
                ");\n" +
                "\n" +
                "timedTransition TE2(\n" +
                "inputs = [UPS_ON2, UPS_ON, equipeManutencao],\n" +
                "outputs = [P1, UPS_ON],\n" +
                "delay = "+slaPreventiva+"\n" +
                ");\n" +
                "\n" +
                "timedTransition TE3(\n" +
                "inputs = [P1],\n" +
                "outputs = [UPS_ON2, equipeManutencao],\n" +
                "delay = 4.0\n" +
                ");\n" +
                "\n" +
                "timedTransition clockPrimeiroAno(\n" +
                "inputs = [inicial],\n" +
                "outputs = [primeiroAno],\n" +
                "delay = 8760.0\n" +
                ");\n" +
                "\n" +
                "timedTransition clockQuartoAno(\n" +
                "inputs = [terceiroAno],\n" +
                "outputs = [quartoAno],\n" +
                "delay = 8760.0\n" +
                ");\n" +
                "\n" +
                "timedTransition clockQuintoAno(\n" +
                "inputs = [quartoAno],\n" +
                "outputs = [quintoAno],\n" +
                "delay = 8760.0\n" +
                ");\n" +
                "\n" +
                "timedTransition clockSegundoAno(\n" +
                "inputs = [primeiroAno],\n" +
                "outputs = [segundoAno],\n" +
                "delay = 8760.0\n" +
                ");\n" +
                "\n" +
                "timedTransition clockTerceiroAno(\n" +
                "inputs = [segundoAno],\n" +
                "outputs = [terceiroAno],\n" +
                "delay = 8760.0\n" +
                ");\n" +
                "\n" +
                "metric av = stationaryAnalysis( expression = \"P{((#UPS_ON=1)OR(#UPS_ON2=1))AND(#STS_ON=1)AND(#SDT_ON=1)AND(#SUB_ON=1)AND(#POWE_ON=1)}\" );\n" +
                "}\n" +
                "\n" +
                "main {\n" +
                "av = solve( Model,av );\n" +
                "println(av);\n" +
                "\n" +
                "}";
    }
    
    static double executarScriptA2(String equipamentoUps, String equipamentoTransformer, String equipamentoSubpainel, String equipamentoPowerstrip, String equipamentoSts, String ups, String sdt, String subpainel, String powerstrip, String sts, String qtEquipe, int sla, int slaPreventiva) throws IOException {
       
        ExecutionRuntime runtime = new ExecutionRuntime();
        //System.out.println(script(equipamentoUps, ups));
        Script script;
        script = new Script(scriptA2(equipamentoUps, equipamentoTransformer, equipamentoSubpainel, equipamentoPowerstrip, equipamentoSts, ups, sdt, subpainel, powerstrip, sts, qtEquipe, sla, slaPreventiva));
        //System.out.println(script);
        //System.exit(0);
        runtime.evaluateScript(script);

        double av;
        Metric Aval;

        Aval = runtime.getModel("Model").getMetric("av");
        av = Aval.solve();
        
        //escrever no arquivo
        //BufferedWriter bw = new BufferedWriter(new FileWriter("teste.txt", true));
        
        // Escreve e fecha arquivo
        //bw.write(av + "\t");
        //bw.close();
        return av;
    }
        
    static String scriptA3(String equipamentoUps, String equipamentoTransformer, String equipamentoSubpainel, String equipamentoPowerstrip, String equipamentoSts, String ups, String sdt, String subpainel, String powerstrip, String sts, String qtEquipe, int sla, int slaPreventiva) {

        return "SPN Model{\n" +
            "\n" +
            "place P0;\n" +
            "place P2;\n" +
            "place P4;\n" +
            "place P6;\n" +
            "place POWE_OFF;\n" +
            "place POWE_ON( tokens= 1 );\n" +
            "place REPARAR_POWE;\n" +
            "place REPARAR_SDT;\n" +
            "place REPARAR_SDT2;\n" +
            "place REPARAR_STS;\n" +
            "place REPARAR_SUB;\n" +
            "place REPARAR_UPS;\n" +
            "place REPARAR_UPS2;\n" +
            "place SDT_OFF;\n" +
            "place SDT_OFF2;\n" +
            "place SDT_ON( tokens= 1 );\n" +
            "place SDT_ON2( tokens= 1 );\n" +
            "place STS_OFF;\n" +
            "place STS_ON( tokens= 1 );\n" +
            "place SUB_OFF;\n" +
            "place SUB_ON( tokens= 1 );\n" +
            "place UPS_OFF;\n" +
            "place UPS_OFF2;\n" +
            "place UPS_ON( tokens= 1 );\n" +
            "place UPS_ON2( tokens= 1 );\n" +
            "place equipeManutencao(tokens="+qtEquipe+");\n" +
            "place inicial( tokens= 1 );\n" +
            "place primeiroAno;\n" +
            "place quartoAno;\n" +
            "place quintoAno;\n" +
            "place segundoAno;\n" +
            "place terceiroAno;\n" +
            "\n" +
            "\n" +
            "immediateTransition TI0(\n" +
            "enablingFunction = \"(#P0=1)OR(#P2=1)OR(#P4=1)OR(#P6=1)\",\n" +
            "inputs = [primeiroAno],\n" +
            "outputs = [inicial]\n" +
            ");\n" +
            "\n" +
            "immediateTransition TI1(\n" +
            "enablingFunction = \"(#P0=1)OR(#P2=1)OR(#P4=1)OR(#P6=1)\",\n" +
            "inputs = [segundoAno],\n" +
            "outputs = [inicial]\n" +
            ");\n" +
            "\n" +
            "immediateTransition TI2(\n" +
            "enablingFunction = \"(#P0=1)OR(#P2=1)OR(#P4=1)OR(#P6=1)\",\n" +
            "inputs = [terceiroAno],\n" +
            "outputs = [inicial]\n" +
            ");\n" +
            "\n" +
            "immediateTransition TI3(\n" +
            "enablingFunction = \"(#P0=1)OR(#P2=1)OR(#P4=1)OR(#P6=1)\",\n" +
            "inputs = [quartoAno],\n" +
            "outputs = [inicial]\n" +
            ");\n" +
            "\n" +
            "immediateTransition TI4(\n" +
            "enablingFunction = \"(#P0=1)OR(#P2=1)OR(#P4=1)OR(#P6=1)\",\n" +
            "inputs = [quintoAno],\n" +
            "outputs = [inicial]\n" +
            ");\n" +
            "\n" +
            "immediateTransition acionarManutencaoPOWE(\n" +
            "inputs = [POWE_OFF, equipeManutencao],\n" +
            "outputs = [REPARAR_POWE]\n" +
            ");\n" +
            "\n" +
            "immediateTransition acionarManutencaoSDT(\n" +
            "inputs = [SDT_OFF, equipeManutencao],\n" +
            "outputs = [REPARAR_SDT]\n" +
            ");\n" +
            "\n" +
            "immediateTransition acionarManutencaoSDT2(\n" +
            "inputs = [SDT_OFF2, equipeManutencao],\n" +
            "outputs = [REPARAR_SDT2]\n" +
            ");\n" +
            "\n" +
            "immediateTransition acionarManutencaoSTS(\n" +
            "inputs = [STS_OFF, equipeManutencao],\n" +
            "outputs = [REPARAR_STS]\n" +
            ");\n" +
            "\n" +
            "immediateTransition acionarManutencaoSUB(\n" +
            "inputs = [SUB_OFF, equipeManutencao],\n" +
            "outputs = [REPARAR_SUB]\n" +
            ");\n" +
            "\n" +
            "immediateTransition acionarManutencaoUPS(\n" +
            "inputs = [UPS_OFF, equipeManutencao],\n" +
            "outputs = [REPARAR_UPS]\n" +
            ");\n" +
            "\n" +
            "immediateTransition acionarManutencaoUPS2(\n" +
            "inputs = [UPS_OFF2, equipeManutencao],\n" +
            "outputs = [REPARAR_UPS2]\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTF_POWE(\n" +
            "inputs = [POWE_ON],\n" +
            "outputs = [POWE_OFF],\n" +
            "delay = \""+equipamentoPowerstrip+"-("+equipamentoPowerstrip+"*0.1*#primeiroAno)-("+equipamentoPowerstrip+"*0.2*#segundoAno)-("+equipamentoPowerstrip+"*0.3*#terceiroAno)-("+equipamentoPowerstrip+"*0.4*#quartoAno)-("+equipamentoPowerstrip+"*0.5*#quintoAno)\"\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTF_SDT(\n" +
            "inputs = [SDT_ON],\n" +
            "outputs = [SDT_OFF],\n" +
            "delay = \""+equipamentoTransformer+"-("+equipamentoTransformer+"*0.1*#primeiroAno)-("+equipamentoTransformer+"*0.2*#segundoAno)-("+equipamentoTransformer+"*0.3*#terceiroAno)-("+equipamentoTransformer+"*0.4*#quartoAno)-("+equipamentoTransformer+"*0.5*#quintoAno)\"\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTF_SDT2(\n" +
            "inputs = [SDT_ON2],\n" +
            "outputs = [SDT_OFF2],\n" +
            "delay = \""+equipamentoTransformer+"-("+equipamentoTransformer+"*0.1*#primeiroAno)-("+equipamentoTransformer+"*0.2*#segundoAno)-("+equipamentoTransformer+"*0.3*#terceiroAno)-("+equipamentoTransformer+"*0.4*#quartoAno)-("+equipamentoTransformer+"*0.5*#quintoAno)\"\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTF_STS(\n" +
            "inputs = [STS_ON],\n" +
            "outputs = [STS_OFF],\n" +
            "delay = \""+equipamentoSts+"-("+equipamentoSts+"*0.1*#primeiroAno)-("+equipamentoSts+"*0.2*#segundoAno)-("+equipamentoSts+"*0.3*#terceiroAno)-("+equipamentoSts+"*0.4*#quartoAno)-("+equipamentoSts+"*0.5*#quintoAno)\"\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTF_SUB(\n" +
            "inputs = [SUB_ON],\n" +
            "outputs = [SUB_OFF],\n" +
            "delay = \""+equipamentoSubpainel+"-("+equipamentoSubpainel+"*0.1*#primeiroAno)-("+equipamentoSubpainel+"*0.2*#segundoAno)-("+equipamentoSubpainel+"*0.3*#terceiroAno)-("+equipamentoSubpainel+"*0.4*#quartoAno)-("+equipamentoSubpainel+"*0.5*#quintoAno)\"\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTF_UPS(\n" +
            "inputs = [UPS_ON, UPS_ON],\n" +
            "outputs = [UPS_OFF],\n" +
            "delay = \""+equipamentoUps+"-("+equipamentoUps+"*0.1*#primeiroAno)-("+equipamentoUps+"*0.2*#segundoAno)-("+equipamentoUps+"*0.3*#terceiroAno)-("+equipamentoUps+"*0.4*#quartoAno)-("+equipamentoUps+"*0.5*#quintoAno)\"\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTF_UPS2(\n" +
            "inputs = [UPS_ON2],\n" +
            "outputs = [UPS_OFF2],\n" +
            "delay = \""+equipamentoUps+"-("+equipamentoUps+"*0.1*#primeiroAno)-("+equipamentoUps+"*0.2*#segundoAno)-("+equipamentoUps+"*0.3*#terceiroAno)-("+equipamentoUps+"*0.4*#quartoAno)-("+equipamentoUps+"*0.5*#quintoAno)\"\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTR_POWE(\n" +
            "inputs = [REPARAR_POWE],\n" +
            "outputs = [POWE_ON, equipeManutencao],\n" +
            "delay = 8.0\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTR_SDT(\n" +
            "inputs = [REPARAR_SDT],\n" +
            "outputs = [SDT_ON, equipeManutencao],\n" +
            "delay = "+sla+"\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTR_SDT2(\n" +
            "inputs = [REPARAR_SDT2],\n" +
            "outputs = [SDT_ON2, equipeManutencao],\n" +
            "delay = "+sla+"\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTR_STS(\n" +
            "inputs = [REPARAR_STS],\n" +
            "outputs = [STS_ON, equipeManutencao],\n" +
            "delay = 8.0\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTR_SUB(\n" +
            "inputs = [REPARAR_SUB],\n" +
            "outputs = [SUB_ON, equipeManutencao],\n" +
            "delay = 8.0\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTR_UPS(\n" +
            "inputs = [REPARAR_UPS],\n" +
            "outputs = [UPS_ON, equipeManutencao],\n" +
            "delay = "+sla+"\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTR_UPS2(\n" +
            "inputs = [REPARAR_UPS2],\n" +
            "outputs = [UPS_ON2, equipeManutencao],\n" +
            "delay = "+sla+"\n" +
            ");\n" +
            "\n" +
            "timedTransition TE0(\n" +
            "inputs = [UPS_ON, UPS_ON2, SDT_ON2, equipeManutencao],\n" +
            "outputs = [P0, UPS_ON2, SDT_ON2],\n" +
            "delay = "+slaPreventiva+"\n" +
            ");\n" +
            "\n" +
            "timedTransition TE1(\n" +
            "inputs = [P0],\n" +
            "outputs = [UPS_ON, equipeManutencao],\n" +
            "delay = 4.0\n" +
            ");\n" +
            "\n" +
            "timedTransition TE2(\n" +
            "inputs = [UPS_ON2, UPS_ON, SDT_ON, equipeManutencao],\n" +
            "outputs = [P2, UPS_ON, SDT_ON],\n" +
            "delay = "+slaPreventiva+"\n" +
            ");\n" +
            "\n" +
            "timedTransition TE3(\n" +
            "inputs = [P2],\n" +
            "outputs = [UPS_ON2, equipeManutencao],\n" +
            "delay = 4.0\n" +
            ");\n" +
            "\n" +
            "timedTransition TE4(\n" +
            "inputs = [SDT_ON, SDT_ON2, UPS_ON2, equipeManutencao],\n" +
            "outputs = [P4, SDT_ON2, UPS_ON2],\n" +
            "delay = "+slaPreventiva+"\n" +
            ");\n" +
            "\n" +
            "timedTransition TE5(\n" +
            "inputs = [P4],\n" +
            "outputs = [SDT_ON, equipeManutencao],\n" +
            "delay = 4.0\n" +
            ");\n" +
            "\n" +
            "timedTransition TE6(\n" +
            "inputs = [SDT_ON2, SDT_ON, UPS_ON, equipeManutencao],\n" +
            "outputs = [P6, SDT_ON, UPS_ON],\n" +
            "delay = "+slaPreventiva+"\n" +
            ");\n" +
            "\n" +
            "timedTransition TE7(\n" +
            "inputs = [P6],\n" +
            "outputs = [SDT_ON2, equipeManutencao],\n" +
            "delay = 4.0\n" +
            ");\n" +
            "\n" +
            "timedTransition clockPrimeiroAno(\n" +
            "inputs = [inicial],\n" +
            "outputs = [primeiroAno],\n" +
            "delay = 8760.0\n" +
            ");\n" +
            "\n" +
            "timedTransition clockQuartoAno(\n" +
            "inputs = [terceiroAno],\n" +
            "outputs = [quartoAno],\n" +
            "delay = 8760.0\n" +
            ");\n" +
            "\n" +
            "timedTransition clockQuintoAno(\n" +
            "inputs = [quartoAno],\n" +
            "outputs = [quintoAno],\n" +
            "delay = 8760.0\n" +
            ");\n" +
            "\n" +
            "timedTransition clockSegundoAno(\n" +
            "inputs = [primeiroAno],\n" +
            "outputs = [segundoAno],\n" +
            "delay = 8760.0\n" +
            ");\n" +
            "\n" +
            "timedTransition clockTerceiroAno(\n" +
            "inputs = [segundoAno],\n" +
            "outputs = [terceiroAno],\n" +
            "delay = 8760.0\n" +
            ");\n" +
            "\n" +
            "metric av = stationaryAnalysis( expression = \"P{(((#UPS_ON=1)AND(#SDT_ON=1))OR((#UPS_ON2=1)AND(#SDT_ON2=1)))AND(#STS_ON=1)AND(#SUB_ON=1)AND(#POWE_ON=1)}\" );\n" +
            "}\n" +
            "\n" +
            "main {\n" +
            "av = solve( Model,av );\n" +
            "println(av);\n" +
            "\n" +
            "}";
    }
    
    static double executarScriptA3(String equipamentoUps, String equipamentoTransformer, String equipamentoSubpainel, String equipamentoPowerstrip, String equipamentoSts, String ups, String sdt, String subpainel, String powerstrip, String sts, String qtEquipe, int sla, int slaPreventiva) throws IOException {
       
        ExecutionRuntime runtime = new ExecutionRuntime();
        //System.out.println(script(equipamentoUps, ups));
        Script script;
        script = new Script(scriptA3(equipamentoUps, equipamentoTransformer, equipamentoSubpainel, equipamentoPowerstrip, equipamentoSts, ups, sdt, subpainel, powerstrip, sts,qtEquipe, sla, slaPreventiva));
        //System.out.println(script);
        //System.exit(0);
        runtime.evaluateScript(script);

        double av;
        Metric Aval;

        Aval = runtime.getModel("Model").getMetric("av");
        av = Aval.solve();
        
        //escrever no arquivo
        //BufferedWriter bw = new BufferedWriter(new FileWriter("teste.txt", true));
        
        // Escreve e fecha arquivo
        //bw.write(av + "\t");
        //bw.close();
        return av;
    }
        
static String scriptA4(String equipamentoUps, String equipamentoTransformer, String equipamentoSubpainel, String equipamentoPowerstrip, String equipamentoSts, String ups, String sdt, String subpainel, String powerstrip, String sts, String qtEquipe, int sla, int slaPreventiva) {

    return "SPN Model{\n" +
            "\n" +
            "place P0;\n" +
            "place P10;\n" +
            "place P11;\n" +
            "place P2;\n" +
            "place P4;\n" +
            "place P6;\n" +
            "place POWE_OFF;\n" +
            "place POWE_ON( tokens= 1 );\n" +
            "place REPARAR_POWE;\n" +
            "place REPARAR_SDT;\n" +
            "place REPARAR_SDT2;\n" +
            "place REPARAR_STS;\n" +
            "place REPARAR_SUB;\n" +
            "place REPARAR_SUB2;\n" +
            "place REPARAR_UPS;\n" +
            "place REPARAR_UPS2;\n" +
            "place SDT_OFF;\n" +
            "place SDT_OFF2;\n" +
            "place SDT_ON( tokens= 1 );\n" +
            "place SDT_ON2( tokens= 1 );\n" +
            "place STS_OFF;\n" +
            "place STS_ON( tokens= 1 );\n" +
            "place SUB_OFF;\n" +
            "place SUB_OFF2;\n" +
            "place SUB_ON( tokens= 1 );\n" +
            "place SUB_ON2( tokens= 1 );\n" +
            "place UPS_OFF;\n" +
            "place UPS_OFF2;\n" +
            "place UPS_ON( tokens= 1 );\n" +
            "place UPS_ON2( tokens= 1 );\n" +
            "place equipeManutencao( tokens="+qtEquipe+");\n" +
            "place inicial( tokens= 1 );\n" +
            "place primeiroAno;\n" +
            "place quartoAno;\n" +
            "place quintoAno;\n" +
            "place segundoAno;\n" +
            "place terceiroAno;\n" +
            "\n" +
            "\n" +
            "immediateTransition TI0(\n" +
            "enablingFunction = \"(#P0=1)OR(#P2=1)OR(#P4=1)OR(#P6=1)OR(#P10=1)OR(#P11=1)\",\n" +
            "inputs = [primeiroAno],\n" +
            "outputs = [inicial]\n" +
            ");\n" +
            "\n" +
            "immediateTransition TI1(\n" +
            "enablingFunction = \"(#P0=1)OR(#P2=1)OR(#P4=1)OR(#P6=1)OR(#P10=1)OR(#P11=1)\",\n" +
            "inputs = [segundoAno],\n" +
            "outputs = [inicial]\n" +
            ");\n" +
            "\n" +
            "immediateTransition TI2(\n" +
            "enablingFunction = \"(#P0=1)OR(#P2=1)OR(#P4=1)OR(#P6=1)OR(#P10=1)OR(#P11=1)\",\n" +
            "inputs = [terceiroAno],\n" +
            "outputs = [inicial]\n" +
            ");\n" +
            "\n" +
            "immediateTransition TI3(\n" +
            "enablingFunction = \"(#P0=1)OR(#P2=1)OR(#P4=1)OR(#P6=1)OR(#P10=1)OR(#P11=1)\",\n" +
            "inputs = [quartoAno],\n" +
            "outputs = [inicial]\n" +
            ");\n" +
            "\n" +
            "immediateTransition TI4(\n" +
            "enablingFunction = \"(#P0=1)OR(#P2=1)OR(#P4=1)OR(#P6=1)OR(#P10=1)OR(#P11=1)\",\n" +
            "inputs = [quintoAno],\n" +
            "outputs = [inicial]\n" +
            ");\n" +
            "\n" +
            "immediateTransition acionarManutencaoPOWE(\n" +
            "inputs = [POWE_OFF, equipeManutencao],\n" +
            "outputs = [REPARAR_POWE]\n" +
            ");\n" +
            "\n" +
            "immediateTransition acionarManutencaoSDT(\n" +
            "inputs = [SDT_OFF, equipeManutencao],\n" +
            "outputs = [REPARAR_SDT]\n" +
            ");\n" +
            "\n" +
            "immediateTransition acionarManutencaoSDT2(\n" +
            "inputs = [SDT_OFF2, equipeManutencao],\n" +
            "outputs = [REPARAR_SDT2]\n" +
            ");\n" +
            "\n" +
            "immediateTransition acionarManutencaoSTS(\n" +
            "inputs = [STS_OFF, equipeManutencao],\n" +
            "outputs = [REPARAR_STS]\n" +
            ");\n" +
            "\n" +
            "immediateTransition acionarManutencaoSUB(\n" +
            "inputs = [SUB_OFF, equipeManutencao],\n" +
            "outputs = [REPARAR_SUB]\n" +
            ");\n" +
            "\n" +
            "immediateTransition acionarManutencaoSUB2(\n" +
            "inputs = [SUB_OFF2, equipeManutencao],\n" +
            "outputs = [REPARAR_SUB2]\n" +
            ");\n" +
            "\n" +
            "immediateTransition acionarManutencaoUPS(\n" +
            "inputs = [UPS_OFF, equipeManutencao],\n" +
            "outputs = [REPARAR_UPS]\n" +
            ");\n" +
            "\n" +
            "immediateTransition acionarManutencaoUPS2(\n" +
            "inputs = [UPS_OFF2, equipeManutencao],\n" +
            "outputs = [REPARAR_UPS2]\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTF_POWE(\n" +
            "inputs = [POWE_ON],\n" +
            "outputs = [POWE_OFF],\n" +
            "delay = \""+equipamentoPowerstrip+"-("+equipamentoPowerstrip+"*0.1*#primeiroAno)-("+equipamentoPowerstrip+"*0.2*#segundoAno)-("+equipamentoPowerstrip+"*0.3*#terceiroAno)-("+equipamentoPowerstrip+"*0.4*#quartoAno)-("+equipamentoPowerstrip+"*0.5*#quintoAno)\"\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTF_SDT(\n" +
            "inputs = [SDT_ON],\n" +
            "outputs = [SDT_OFF],\n" +
            "delay = \""+equipamentoTransformer+"-("+equipamentoTransformer+"*0.1*#primeiroAno)-("+equipamentoTransformer+"*0.2*#segundoAno)-("+equipamentoTransformer+"*0.3*#terceiroAno)-("+equipamentoTransformer+"*0.4*#quartoAno)-("+equipamentoTransformer+"*0.5*#quintoAno)\"\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTF_SDT2(\n" +
            "inputs = [SDT_ON2],\n" +
            "outputs = [SDT_OFF2],\n" +
            "delay = \""+equipamentoTransformer+"-("+equipamentoTransformer+"*0.1*#primeiroAno)-("+equipamentoTransformer+"*0.2*#segundoAno)-("+equipamentoTransformer+"*0.3*#terceiroAno)-("+equipamentoTransformer+"*0.4*#quartoAno)-("+equipamentoTransformer+"*0.5*#quintoAno)\"\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTF_STS(\n" +
            "inputs = [STS_ON],\n" +
            "outputs = [STS_OFF],\n" +
            "delay = \""+equipamentoSts+"-("+equipamentoSts+"*0.1*#primeiroAno)-("+equipamentoSts+"*0.2*#segundoAno)-("+equipamentoSts+"*0.3*#terceiroAno)-("+equipamentoSts+"*0.4*#quartoAno)-("+equipamentoSts+"*0.5*#quintoAno)\"\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTF_SUB(\n" +
            "inputs = [SUB_ON],\n" +
            "outputs = [SUB_OFF],\n" +
            "delay = \""+equipamentoSubpainel+"-("+equipamentoSubpainel+"*0.1*#primeiroAno)-("+equipamentoSubpainel+"*0.2*#segundoAno)-("+equipamentoSubpainel+"*0.3*#terceiroAno)-("+equipamentoSubpainel+"*0.4*#quartoAno)-("+equipamentoSubpainel+"*0.5*#quintoAno)\"\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTF_SUB2(\n" +
            "inputs = [SUB_ON2],\n" +
            "outputs = [SUB_OFF2],\n" +
            "delay = \""+equipamentoSubpainel+"-("+equipamentoSubpainel+"*0.1*#primeiroAno)-("+equipamentoSubpainel+"*0.2*#segundoAno)-("+equipamentoSubpainel+"*0.3*#terceiroAno)-("+equipamentoSubpainel+"*0.4*#quartoAno)-("+equipamentoSubpainel+"*0.5*#quintoAno)\"\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTF_UPS(\n" +
            "inputs = [UPS_ON, UPS_ON],\n" +
            "outputs = [UPS_OFF],\n" +
            "delay = \""+equipamentoUps+"-("+equipamentoUps+"*0.1*#primeiroAno)-("+equipamentoUps+"*0.2*#segundoAno)-("+equipamentoUps+"*0.3*#terceiroAno)-("+equipamentoUps+"*0.4*#quartoAno)-("+equipamentoUps+"*0.5*#quintoAno)\"\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTF_UPS2(\n" +
            "inputs = [UPS_ON2],\n" +
            "outputs = [UPS_OFF2],\n" +
            "delay = \""+equipamentoUps+"-("+equipamentoUps+"*0.1*#primeiroAno)-("+equipamentoUps+"*0.2*#segundoAno)-("+equipamentoUps+"*0.3*#terceiroAno)-("+equipamentoUps+"*0.4*#quartoAno)-("+equipamentoUps+"*0.5*#quintoAno)\"\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTR_POWE(\n" +
            "inputs = [REPARAR_POWE],\n" +
            "outputs = [POWE_ON, equipeManutencao],\n" +
            "delay = 8.0\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTR_SDT(\n" +
            "inputs = [REPARAR_SDT],\n" +
            "outputs = [SDT_ON, equipeManutencao],\n" +
            "delay = "+sla+"\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTR_SDT2(\n" +
            "inputs = [REPARAR_SDT2],\n" +
            "outputs = [SDT_ON2, equipeManutencao],\n" +
            "delay = "+sla+"\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTR_STS(\n" +
            "inputs = [REPARAR_STS],\n" +
            "outputs = [STS_ON, equipeManutencao],\n" +
            "delay = 8.0\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTR_SUB(\n" +
            "inputs = [REPARAR_SUB],\n" +
            "outputs = [SUB_ON, equipeManutencao],\n" +
           "delay = "+sla+"\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTR_SUB2(\n" +
            "inputs = [REPARAR_SUB2],\n" +
            "outputs = [SUB_ON2, equipeManutencao],\n" +
            "delay = "+sla+"\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTR_UPS(\n" +
            "inputs = [REPARAR_UPS],\n" +
            "outputs = [UPS_ON, equipeManutencao],\n" +
            "delay = "+sla+"\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTR_UPS2(\n" +
            "inputs = [REPARAR_UPS2],\n" +
            "outputs = [UPS_ON2, equipeManutencao],\n" +
            "delay = "+sla+"\n" +
            ");\n" +
            "\n" +
            "timedTransition TE0(\n" +
            "inputs = [UPS_ON, UPS_ON2, SDT_ON2, SUB_ON2, equipeManutencao],\n" +
            "outputs = [P0, UPS_ON2, SDT_ON2, SUB_ON2],\n" +
            "delay = "+slaPreventiva+"\n" +
            ");\n" +
            "\n" +
            "timedTransition TE1(\n" +
            "inputs = [P0],\n" +
            "outputs = [UPS_ON, equipeManutencao],\n" +
            "delay = 4.0\n" +
            ");\n" +
            "\n" +
            "timedTransition TE10(\n" +
            "inputs = [SUB_ON2, SUB_ON, UPS_ON, SDT_ON, equipeManutencao],\n" +
            "outputs = [P10, SUB_ON, UPS_ON, SDT_ON],\n" +
           "delay = "+slaPreventiva+"\n" +
            ");\n" +
            "\n" +
            "timedTransition TE11(\n" +
            "inputs = [P10],\n" +
            "outputs = [SUB_ON2, equipeManutencao],\n" +
            "delay = 4.0\n" +
            ");\n" +
            "\n" +
            "timedTransition TE12(\n" +
            "inputs = [SUB_ON, SUB_ON2, UPS_ON2, SDT_ON2, equipeManutencao],\n" +
            "outputs = [P11, SUB_ON2, UPS_ON2, SDT_ON2],\n" +
            "delay = "+slaPreventiva+"\n" +
            ");\n" +
            "\n" +
            "timedTransition TE13(\n" +
            "inputs = [P11],\n" +
            "outputs = [SUB_ON, equipeManutencao],\n" +
            "delay = 4.0\n" +
            ");\n" +
            "\n" +
            "timedTransition TE2(\n" +
            "inputs = [UPS_ON2, UPS_ON, SDT_ON, SUB_ON, equipeManutencao],\n" +
            "outputs = [P2, UPS_ON, SDT_ON, SUB_ON],\n" +
           "delay = "+slaPreventiva+"\n" +
            ");\n" +
            "\n" +
            "timedTransition TE3(\n" +
            "inputs = [P2],\n" +
            "outputs = [UPS_ON2, equipeManutencao],\n" +
            "delay = 4.0\n" +
            ");\n" +
            "\n" +
            "timedTransition TE4(\n" +
            "inputs = [SDT_ON, SDT_ON2, UPS_ON2, SUB_ON2, equipeManutencao],\n" +
            "outputs = [P4, SDT_ON2, UPS_ON2, SUB_ON2],\n" +
            "delay = "+slaPreventiva+"\n" +
            ");\n" +
            "\n" +
            "timedTransition TE5(\n" +
            "inputs = [P4],\n" +
            "outputs = [SDT_ON, equipeManutencao],\n" +
            "delay = 4.0\n" +
            ");\n" +
            "\n" +
            "timedTransition TE6(\n" +
            "inputs = [SDT_ON2, SDT_ON, UPS_ON, SUB_ON, equipeManutencao],\n" +
            "outputs = [P6, SDT_ON, UPS_ON, SUB_ON],\n" +
            "delay = "+slaPreventiva+"\n" +
            ");\n" +
            "\n" +
            "timedTransition TE7(\n" +
            "inputs = [P6],\n" +
            "outputs = [SDT_ON2, equipeManutencao],\n" +
            "delay = 4.0\n" +
            ");\n" +
            "\n" +
            "timedTransition clockPrimeiroAno(\n" +
            "inputs = [inicial],\n" +
            "outputs = [primeiroAno],\n" +
            "delay = 8760.0\n" +
            ");\n" +
            "\n" +
            "timedTransition clockQuartoAno(\n" +
            "inputs = [terceiroAno],\n" +
            "outputs = [quartoAno],\n" +
            "delay = 8760.0\n" +
            ");\n" +
            "\n" +
            "timedTransition clockQuintoAno(\n" +
            "inputs = [quartoAno],\n" +
            "outputs = [quintoAno],\n" +
            "delay = 8760.0\n" +
            ");\n" +
            "\n" +
            "timedTransition clockSegundoAno(\n" +
            "inputs = [primeiroAno],\n" +
            "outputs = [segundoAno],\n" +
            "delay = 8760.0\n" +
            ");\n" +
            "\n" +
            "timedTransition clockTerceiroAno(\n" +
            "inputs = [segundoAno],\n" +
            "outputs = [terceiroAno],\n" +
            "delay = 8760.0\n" +
            ");\n" +
            "\n" +
            "metric av = stationaryAnalysis( expression = \"P{(((#UPS_ON=1)AND(#SDT_ON=1)AND(#SUB_ON=1))OR((#UPS_ON2=1)AND(#SDT_ON2=1)AND(#SUB_ON2=1)))AND(#STS_ON=1)AND(#POWE_ON=1)}\" );\n" +
            "}\n" +
            "\n" +
            "main {\n" +
            "av = solve( Model,av );\n" +
            "println(av);\n" +
            "\n" +
            "}";
    }
    
    static double executarScriptA4(String equipamentoUps, String equipamentoTransformer, String equipamentoSubpainel, String equipamentoPowerstrip, String equipamentoSts, String ups, String sdt, String subpainel, String powerstrip, String sts, String qtEquipe, int sla, int slaPreventiva) throws IOException {
       
        ExecutionRuntime runtime = new ExecutionRuntime();
        //System.out.println(script(equipamentoUps, ups));
        Script script;
        script = new Script(scriptA4(equipamentoUps, equipamentoTransformer, equipamentoSubpainel, equipamentoPowerstrip, equipamentoSts, ups, sdt, subpainel, powerstrip, sts, qtEquipe, sla, slaPreventiva));
        //System.out.println(script);
        //System.exit(0);s
        runtime.evaluateScript(script);

        double av;
        Metric Aval;

        Aval = runtime.getModel("Model").getMetric("av");
        av = Aval.solve();
        
        //escrever no arquivo
        //BufferedWriter bw = new BufferedWriter(new FileWriter("teste.txt", true));
        
        // Escreve e fecha arquivo
        //bw.write(av + "\t");
        //bw.close();
        return av;
    }      

static String scriptA5(String equipamentoUps, String equipamentoTransformer, String equipamentoSubpainel, String equipamentoPowerstrip, String equipamentoSts, String ups, String sdt, String subpainel, String powerstrip, String sts, String qtdEquipe, int sla, int slaPreventiva) {
                    
     return "SPN Model{\n" +
            "\n" +
            "place P0;\n" +
            "place P10;\n" +
            "place P11;\n" +
            "place P12;\n" +
            "place P14;\n" +
            "place P2;\n" +
            "place P4;\n" +
            "place P6;\n" +
            "place POWE_OFF;\n" +
            "place POWE_OFF2;\n" +
            "place POWE_ON( tokens= 1 );\n" +
            "place POWE_ON2( tokens= 1 );\n" +
            "place REPARAR_POWE;\n" +
            "place REPARAR_POWE2;\n" +
            "place REPARAR_SDT;\n" +
            "place REPARAR_SDT2;\n" +
            "place REPARAR_SUB;\n" +
            "place REPARAR_SUB2;\n" +
            "place REPARAR_UPS;\n" +
            "place REPARAR_UPS2;\n" +
            "place SDT_OFF;\n" +
            "place SDT_OFF2;\n" +
            "place SDT_ON( tokens= 1 );\n" +
            "place SDT_ON2( tokens= 1 );\n" +
            "place SUB_OFF;\n" +
            "place SUB_OFF2;\n" +
            "place SUB_ON( tokens= 1 );\n" +
            "place SUB_ON2( tokens= 1 );\n" +
            "place UPS_OFF;\n" +
            "place UPS_OFF2;\n" +
            "place UPS_ON( tokens= 1 );\n" +
            "place UPS_ON2( tokens= 1 );\n" +
            "place equipeManutencao( tokens="+qtdEquipe+");\n" +
            "place inicial( tokens= 1 );\n" +
            "place primeiroAno;\n" +
            "place quartoAno;\n" +
            "place quintoAno;\n" +
            "place segundoAno;\n" +
            "place terceiroAno;\n" +
            "\n" +
            "\n" +
            "immediateTransition TI0(\n" +
            "enablingFunction = \"(#P0=1)OR(#P2=1)OR(#P4=1)OR(#P6=1)OR(#P10=1)OR(#P11=1)OR(#P12=1)OR(#P14=1)\",\n" +
            "inputs = [primeiroAno],\n" +
            "outputs = [inicial]\n" +
            ");\n" +
            "\n" +
            "immediateTransition TI1(\n" +
            "enablingFunction = \"(#P0=1)OR(#P2=1)OR(#P4=1)OR(#P6=1)OR(#P10=1)OR(#P11=1)OR(#P12=1)OR(#P14=1)\",\n" +
            "inputs = [segundoAno],\n" +
            "outputs = [inicial]\n" +
            ");\n" +
            "\n" +
            "immediateTransition TI2(\n" +
            "enablingFunction = \"(#P0=1)OR(#P2=1)OR(#P4=1)OR(#P6=1)OR(#P10=1)OR(#P11=1)OR(#P12=1)OR(#P14=1)\",\n" +
            "inputs = [terceiroAno],\n" +
            "outputs = [inicial]\n" +
            ");\n" +
            "\n" +
            "immediateTransition TI3(\n" +
            "enablingFunction = \"(#P0=1)OR(#P2=1)OR(#P4=1)OR(#P6=1)OR(#P10=1)OR(#P11=1)OR(#P12=1)OR(#P14=1)\",\n" +
            "inputs = [quartoAno],\n" +
            "outputs = [inicial]\n" +
            ");\n" +
            "\n" +
            "immediateTransition TI4(\n" +
            "enablingFunction = \"(#P0=1)OR(#P2=1)OR(#P4=1)OR(#P6=1)OR(#P10=1)OR(#P11=1)OR(#P12=1)OR(#P14=1)\",\n" +
            "inputs = [quintoAno],\n" +
            "outputs = [inicial]\n" +
            ");\n" +
            "\n" +
            "immediateTransition acionarManutencaoPOWE(\n" +
            "inputs = [POWE_OFF, equipeManutencao],\n" +
            "outputs = [REPARAR_POWE]\n" +
            ");\n" +
            "\n" +
            "immediateTransition acionarManutencaoPOWE2(\n" +
            "inputs = [POWE_OFF2, equipeManutencao],\n" +
            "outputs = [REPARAR_POWE2]\n" +
            ");\n" +
            "\n" +
            "immediateTransition acionarManutencaoSDT(\n" +
            "inputs = [SDT_OFF, equipeManutencao],\n" +
            "outputs = [REPARAR_SDT]\n" +
            ");\n" +
            "\n" +
            "immediateTransition acionarManutencaoSDT2(\n" +
            "inputs = [SDT_OFF2, equipeManutencao],\n" +
            "outputs = [REPARAR_SDT2]\n" +
            ");\n" +
            "\n" +
            "immediateTransition acionarManutencaoSUB(\n" +
            "inputs = [SUB_OFF, equipeManutencao],\n" +
            "outputs = [REPARAR_SUB]\n" +
            ");\n" +
            "\n" +
            "immediateTransition acionarManutencaoSUB2(\n" +
            "inputs = [SUB_OFF2, equipeManutencao],\n" +
            "outputs = [REPARAR_SUB2]\n" +
            ");\n" +
            "\n" +
            "immediateTransition acionarManutencaoUPS(\n" +
            "inputs = [UPS_OFF, equipeManutencao],\n" +
            "outputs = [REPARAR_UPS]\n" +
            ");\n" +
            "\n" +
            "immediateTransition acionarManutencaoUPS2(\n" +
            "inputs = [UPS_OFF2, equipeManutencao],\n" +
            "outputs = [REPARAR_UPS2]\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTF_POWE(\n" +
            "inputs = [POWE_ON],\n" +
            "outputs = [POWE_OFF],\n" +
            "delay = \""+equipamentoPowerstrip+"-("+equipamentoPowerstrip+"*0.1*#primeiroAno)-("+equipamentoPowerstrip+"*0.2*#segundoAno)-("+equipamentoPowerstrip+"*0.3*#terceiroAno)-("+equipamentoPowerstrip+"*0.4*#quartoAno)-("+equipamentoPowerstrip+"*0.5*#quintoAno)\"\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTF_POWE2(\n" +
            "inputs = [POWE_ON2],\n" +
            "outputs = [POWE_OFF2],\n" +
            "delay = \""+equipamentoPowerstrip+"-("+equipamentoPowerstrip+"*0.1*#primeiroAno)-("+equipamentoPowerstrip+"*0.2*#segundoAno)-("+equipamentoPowerstrip+"*0.3*#terceiroAno)-("+equipamentoPowerstrip+"*0.4*#quartoAno)-("+equipamentoPowerstrip+"*0.5*#quintoAno)\"\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTF_SDT(\n" +
            "inputs = [SDT_ON],\n" +
            "outputs = [SDT_OFF],\n" +
            "delay = \""+equipamentoTransformer+"-("+equipamentoTransformer+"*0.1*#primeiroAno)-("+equipamentoTransformer+"*0.2*#segundoAno)-("+equipamentoTransformer+"*0.3*#terceiroAno)-("+equipamentoTransformer+"*0.4*#quartoAno)-("+equipamentoTransformer+"*0.5*#quintoAno)\"\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTF_SDT2(\n" +
            "inputs = [SDT_ON2],\n" +
            "outputs = [SDT_OFF2],\n" +
            "delay = \""+equipamentoTransformer+"-("+equipamentoTransformer+"*0.1*#primeiroAno)-("+equipamentoTransformer+"*0.2*#segundoAno)-("+equipamentoTransformer+"*0.3*#terceiroAno)-("+equipamentoTransformer+"*0.4*#quartoAno)-("+equipamentoTransformer+"*0.5*#quintoAno)\"\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTF_SUB(\n" +
            "inputs = [SUB_ON],\n" +
            "outputs = [SUB_OFF],\n" +
            "delay = \""+equipamentoSubpainel+"-("+equipamentoSubpainel+"*0.1*#primeiroAno)-("+equipamentoSubpainel+"*0.2*#segundoAno)-("+equipamentoSubpainel+"*0.3*#terceiroAno)-("+equipamentoSubpainel+"*0.4*#quartoAno)-("+equipamentoSubpainel+"*0.5*#quintoAno)\"\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTF_SUB2(\n" +
            "inputs = [SUB_ON2],\n" +
            "outputs = [SUB_OFF2],\n" +
            "delay = \""+equipamentoSubpainel+"-("+equipamentoSubpainel+"*0.1*#primeiroAno)-("+equipamentoSubpainel+"*0.2*#segundoAno)-("+equipamentoSubpainel+"*0.3*#terceiroAno)-("+equipamentoSubpainel+"*0.4*#quartoAno)-("+equipamentoSubpainel+"*0.5*#quintoAno)\"\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTF_UPS(\n" +
            "inputs = [UPS_ON, UPS_ON],\n" +
            "outputs = [UPS_OFF],\n" +
            "delay = \""+equipamentoUps+"-("+equipamentoUps+"*0.1*#primeiroAno)-("+equipamentoUps+"*0.2*#segundoAno)-("+equipamentoUps+"*0.3*#terceiroAno)-("+equipamentoUps+"*0.4*#quartoAno)-("+equipamentoUps+"*0.5*#quintoAno)\"\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTF_UPS2(\n" +
            "inputs = [UPS_ON2],\n" +
            "outputs = [UPS_OFF2],\n" +
            "delay = \""+equipamentoUps+"-("+equipamentoUps+"*0.1*#primeiroAno)-("+equipamentoUps+"*0.2*#segundoAno)-("+equipamentoUps+"*0.3*#terceiroAno)-("+equipamentoUps+"*0.4*#quartoAno)-("+equipamentoUps+"*0.5*#quintoAno)\"\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTR_POWE(\n" +
            "inputs = [REPARAR_POWE],\n" +
            "outputs = [POWE_ON, equipeManutencao],\n" +
            "delay = "+sla+"\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTR_POWE2(\n" +
            "inputs = [REPARAR_POWE2],\n" +
            "outputs = [POWE_ON2, equipeManutencao],\n" +
            "delay = "+sla+"\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTR_SDT(\n" +
            "inputs = [REPARAR_SDT],\n" +
            "outputs = [SDT_ON, equipeManutencao],\n" +
            "delay = "+sla+"\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTR_SDT2(\n" +
            "inputs = [REPARAR_SDT2],\n" +
            "outputs = [SDT_ON2, equipeManutencao],\n" +
            "delay = "+sla+"\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTR_SUB(\n" +
            "inputs = [REPARAR_SUB],\n" +
            "outputs = [SUB_ON, equipeManutencao],\n" +
            "delay = "+sla+"\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTR_SUB2(\n" +
            "inputs = [REPARAR_SUB2],\n" +
            "outputs = [SUB_ON2, equipeManutencao],\n" +
            "delay = "+sla+"\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTR_UPS(\n" +
            "inputs = [REPARAR_UPS],\n" +
            "outputs = [UPS_ON, equipeManutencao],\n" +
            "delay = "+sla+"\n" +
            ");\n" +
            "\n" +
            "timedTransition MTTR_UPS2(\n" +
            "inputs = [REPARAR_UPS2],\n" +
            "outputs = [UPS_ON2, equipeManutencao],\n" +
            "delay = "+sla+"\n" +
            ");\n" +
            "\n" +
            "timedTransition TE0(\n" +
            "inputs = [UPS_ON, UPS_ON2, SDT_ON2, SUB_ON2, POWE_ON2, equipeManutencao],\n" +
            "outputs = [P0, UPS_ON2, SDT_ON2, SUB_ON2, POWE_ON2],\n" +
            "delay = "+slaPreventiva+"\n" +
            ");\n" +
            "\n" +
            "timedTransition TE1(\n" +
            "inputs = [P0],\n" +
            "outputs = [UPS_ON, equipeManutencao],\n" +
            "delay = 4.0\n" +
            ");\n" +
            "\n" +
            "timedTransition TE10(\n" +
            "inputs = [SUB_ON2, SUB_ON, UPS_ON, SDT_ON, POWE_ON, equipeManutencao],\n" +
            "outputs = [P10, SUB_ON, UPS_ON, SDT_ON, POWE_ON],\n" +
            "delay = "+slaPreventiva+"\n" +
            ");\n" +
            "\n" +
            "timedTransition TE11(\n" +
            "inputs = [P10],\n" +
            "outputs = [SUB_ON2, equipeManutencao],\n" +
            "delay = 4.0\n" +
            ");\n" +
            "\n" +
            "timedTransition TE12(\n" +
            "inputs = [SUB_ON, SUB_ON2, UPS_ON2, SDT_ON2, POWE_ON2, equipeManutencao],\n" +
            "outputs = [P11, SUB_ON2, UPS_ON2, SDT_ON2, POWE_ON2],\n" +
            "delay = "+slaPreventiva+"\n" +
            ");\n" +
            "\n" +
            "timedTransition TE13(\n" +
            "inputs = [P11],\n" +
            "outputs = [SUB_ON, equipeManutencao],\n" +
            "delay = 4.0\n" +
            ");\n" +
            "\n" +
            "timedTransition TE14(\n" +
            "inputs = [POWE_ON2, POWE_ON, UPS_ON, SDT_ON, SUB_ON, equipeManutencao],\n" +
            "outputs = [P12, POWE_ON, UPS_ON, SDT_ON, SUB_ON],\n" +
            "delay = "+slaPreventiva+"\n" +
            ");\n" +
            "\n" +
            "timedTransition TE15(\n" +
            "inputs = [P12],\n" +
            "outputs = [POWE_ON2, equipeManutencao],\n" +
            "delay = 4.0\n" +
            ");\n" +
            "\n" +
            "timedTransition TE16(\n" +
            "inputs = [POWE_ON, POWE_ON2, UPS_ON2, SDT_ON2, SUB_ON2, equipeManutencao],\n" +
            "outputs = [P14, POWE_ON2, UPS_ON2, SDT_ON2, SUB_ON2],\n" +
            "delay = "+slaPreventiva+"\n" +
            ");\n" +
            "\n" +
            "timedTransition TE17(\n" +
            "inputs = [P14],\n" +
            "outputs = [POWE_ON, equipeManutencao],\n" +
            "delay = 4.0\n" +
            ");\n" +
            "\n" +
            "timedTransition TE2(\n" +
            "inputs = [UPS_ON2, UPS_ON, SDT_ON, SUB_ON, POWE_ON, equipeManutencao],\n" +
            "outputs = [P2, UPS_ON, SDT_ON, SUB_ON, POWE_ON],\n" +
            "delay = "+slaPreventiva+"\n" +
            ");\n" +
            "\n" +
            "timedTransition TE3(\n" +
            "inputs = [P2],\n" +
            "outputs = [UPS_ON2, equipeManutencao],\n" +
            "delay = 4.0\n" +
            ");\n" +
            "\n" +
            "timedTransition TE4(\n" +
            "inputs = [SDT_ON, SDT_ON2, UPS_ON2, SUB_ON2, POWE_ON2, equipeManutencao],\n" +
            "outputs = [P4, SDT_ON2, UPS_ON2, SUB_ON2, POWE_ON2],\n" +
            "delay = "+slaPreventiva+"\n" +
            ");\n" +
            "\n" +
            "timedTransition TE5(\n" +
            "inputs = [P4],\n" +
            "outputs = [SDT_ON, equipeManutencao],\n" +
            "delay = 4.0\n" +
            ");\n" +
            "\n" +
            "timedTransition TE6(\n" +
            "inputs = [SDT_ON2, SDT_ON, UPS_ON, SUB_ON, POWE_ON, equipeManutencao],\n" +
            "outputs = [P6, SDT_ON, UPS_ON, SUB_ON, POWE_ON],\n" +
            "delay = "+slaPreventiva+"\n" +
            ");\n" +
            "\n" +
            "timedTransition TE7(\n" +
            "inputs = [P6],\n" +
            "outputs = [SDT_ON2, equipeManutencao],\n" +
            "delay = 4.0\n" +
            ");\n" +
            "\n" +
            "timedTransition clockPrimeiroAno(\n" +
            "inputs = [inicial],\n" +
            "outputs = [primeiroAno],\n" +
            "delay = 8760.0\n" +
            ");\n" +
            "\n" +
            "timedTransition clockQuartoAno(\n" +
            "inputs = [terceiroAno],\n" +
            "outputs = [quartoAno],\n" +
            "delay = 8760.0\n" +
            ");\n" +
            "\n" +
            "timedTransition clockQuintoAno(\n" +
            "inputs = [quartoAno],\n" +
            "outputs = [quintoAno],\n" +
            "delay = 8760.0\n" +
            ");\n" +
            "\n" +
            "timedTransition clockSegundoAno(\n" +
            "inputs = [primeiroAno],\n" +
            "outputs = [segundoAno],\n" +
            "delay = 8760.0\n" +
            ");\n" +
            "\n" +
            "timedTransition clockTerceiroAno(\n" +
            "inputs = [segundoAno],\n" +
            "outputs = [terceiroAno],\n" +
            "delay = 8760.0\n" +
            ");\n" +
            "\n" +
            "metric av = stationaryAnalysis( expression = \"P{(((#UPS_ON=1)AND(#SDT_ON=1)AND(#SUB_ON=1)AND(#POWE_ON=1))OR((#UPS_ON2=1)AND(#SDT_ON2=1)AND(#SUB_ON2=1)AND(#POWE_ON2=1)))}\" );\n" +
            "}\n" +
            "\n" +
            "main {\n" +
            "av = solve( Model,av );\n" +
            "println(av);\n" +
            "\n" +
            "}";
    }
    
        static double executarScriptA5(String equipamentoUps, String equipamentoTransformer, String equipamentoSubpainel, String equipamentoPowerstrip, String equipamentoSts, String ups, String sdt, String subpainel, String powerstrip, String sts, String qtdEquipe, int sla, int slaPreventiva) throws IOException {
       
        ExecutionRuntime runtime = new ExecutionRuntime();
        //System.out.println(script(equipamentoUps, ups));
        Script script;
        script = new Script(scriptA5(equipamentoUps, equipamentoTransformer, equipamentoSubpainel, equipamentoPowerstrip, equipamentoSts, ups, sdt, subpainel, powerstrip, sts, qtdEquipe, sla, slaPreventiva));
        //System.out.println(script);
        //System.exit(0);s
        runtime.evaluateScript(script);

        double av;
        Metric Aval;

        Aval = runtime.getModel("Model").getMetric("av");
        av = Aval.solve();
        
        //escrever no arquivo
        //BufferedWriter bw = new BufferedWriter(new FileWriter("teste.txt", true));
        
        // Escreve e fecha arquivo
        //bw.write(av + "\t");
        //bw.close();
        return av;
    }  

    double executarScriptA2(double executarScriptA2) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


}
